<?php

namespace app\models\forms;

use app\models\Task;
use app\models\User;
use Yii;
use yii\base\Model;
use yii\helpers\BaseHtml;

class TaskRespondForm extends Model
{
    public $userId;
    public $taskId;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['taskId'], 'required'],
            [['userId'], 'required', 'message' => Yii::t('app', 'You must to ' . BaseHtml::a('login', '/site/login'))],
            [['userId', 'taskId'], 'number', 'integerOnly' => true],
            [['taskId'], 'exist', 'targetClass' => 'app\models\Task', 'targetAttribute' => 'id', 'message' => Yii::t('app', 'Task not found')],
            [['userId'], 'exist', 'targetClass' => 'app\models\User', 'targetAttribute' => 'id', 'message' => Yii::t('app', 'User not found')],
            [['userId'], 'userCanRespondValidator'],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'userId' => 'Исполнитель',
            'taskId' => 'Задача',
        ];
    }

    public function userCanRespondValidator($attribute, $params)
    {
        if (!$this->hasErrors('userId') && !$this->hasErrors('taskId')) {

            $user = User::findOne(['id' => $this->userId]);
            $task = Task::findOne(['id' => $this->taskId]);

            if (!$user->getIsPerformer()) {
                $this->addError('userId', Yii::t('app', 'User type is not performer'));
            } elseif ($user->getRespondedToTask($this->taskId)) {
                $this->addError('userId', Yii::t('app', 'User already responded to this task'));
            } elseif ($task->performerId == $user->id) {
                $this->addError('userId', Yii::t('app', 'User already are performer'));
            }
        }
    }
}
