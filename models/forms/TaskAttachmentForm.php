<?php

namespace app\models\forms;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

class TaskAttachmentForm extends Model
{
    /** @var  integer */
    public $id;
    /** @var  UploadedFile */
    public $attachment;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['id'], 'required', 'on' => 'update'],
            [['attachment'], 'required', 'on' => 'insert'],
            [['id'], 'number', 'integerOnly' => true],
            [['attachment'], 'file', 'types' => 'jpg, jpeg, gif, png, txt, doc, docx, xml', 'minSize' => 10, 'maxSize' => 10 * 1024 * 1024],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'attachment' => 'Файл',
        ];
    }
}
