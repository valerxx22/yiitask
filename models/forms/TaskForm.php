<?php

namespace app\models\forms;

use Yii;
use yii\base\Model;

class TaskForm extends Model
{
    public $title;
    public $description;
    public $price;
    public $deadlineNumPart = 1;
    public $deadlineTextPart;
    public $status;
    public $statusAlias;
    public $performerId;
    public $performerName;
    public $categoryIDs;
    public $attachments;

    /**
     * @var string helper attribute to work with tags
     */
    public $tagNames;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [[
                'title',
                'description',
                'price',
                'deadlineNumPart',
                'deadlineTextPart',
                'status',
                'statusAlias',
                'performerId',
                'performerName',
//                'tagNames',
            ], 'required'],
            [['description'], 'string'],
            [['title'], 'string', 'max' => 255],
            [['price'], 'number', 'integerOnly' => true, 'min' => 100, 'max' => 1000000],
            [['deadlineNumPart'], 'number', 'integerOnly' => true, 'min' => 1, 'max' => 100, 'skipOnEmpty' => false],
            [['performerId', 'categoryId'], 'number', 'integerOnly' => true],
//            [['categoryIDs'], 'filter', 'filter' => array( $this, 'filterCategoryIDs' )],
            [['categoryIDs'], 'categoryIDsValidator', 'skipOnEmpty' => false],
            [['attachments'], 'default', 'value' => []],
            [['attachments'], 'attachmentsValidator', 'skipOnEmpty' => true],
            [['tagNames', 'attachments'], 'safe'],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'title' => 'Заголовок',
            'description' => 'Описание',
            'price' => 'Цена',
            'deadlineNumPart' => 'Кол-во',
            'deadlineTextPart' => 'Период',
            'statusAlias' => 'Статус',
            'performerName' => 'Исполнитель',
            'tagNames' => 'Теги',
            'categoryIDs' => 'Категории',
            'attachments' => 'Файлы',
        ];
    }

    public function beforeValidate()
    {
        if (!parent::beforeValidate()) {
            return false;
        }

        if ($this->price !== '') {
            $this->price = preg_replace("/[^0-9]/", "", $this->price);
        }

        return true;
    }

    public function categoryIDsValidator($attribute, $params)
    {
        $selectedItemCount = 0;

        foreach ($this->categoryIDs as $eachCatId) {
            if (is_numeric($eachCatId) && $eachCatId > 0) {
                $selectedItemCount++;
            }
        }

        if ($selectedItemCount == 0) {
            $this->addError($attribute, Yii::t('app', 'Select at least one category.'));
        }
    }

    public function attachmentsValidator($attribute, $params)
    {
        var_dump($this->attachments);
        exit('e1');
    }

    /*public function filterCategoryIDs($categoryIDs)
    {
        $filtered = [];
        foreach ($categoryIDs as $i => $eachCatId) {
            if (is_numeric($eachCatId) && $eachCatId > 0) {
                $filtered[$i] = $eachCatId;
            }
        }
        return $filtered;
    }*/
}
