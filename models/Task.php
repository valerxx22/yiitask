<?php

namespace app\models;

use dosamigos\taggable\Taggable;
use Yii;
use yii\db\Query;

/**
 * This is the model class for table "task".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property string $price
 * @property integer $negotiatedPrice
 * @property integer $deadlineNumPart
 * @property string $deadlineTextPart
 * @property integer $authorId
 * @property integer $performerId
 * @property integer $responseCount
 * @property integer $viewCount
 * @property integer $uniqueViewCount
 * @property string $status
 * @property string $createdDate
 * @property string $lastUpdatedDate
 *
 * @property User $performer
 * @property User $author
 * @property Tag[] $tags
 * @property TaskCategory[] $categories
 */
class Task extends \yii\db\ActiveRecord
{
    const STATUS_NEW = 'new';
    const STATUS_OPENED = 'opened';
    const STATUS_REOPENED = 'reopened';
    const STATUS_IN_PROGRESS = 'inProgress';
    const STATUS_RESOLVED = 'resolved';
    const STATUS_VERIFIED = 'verified';
    const STATUS_CLOSED = 'closed';

    static $statuses = [
        self::STATUS_NEW => 'Новый',
        self::STATUS_OPENED => 'Открытый',
        self::STATUS_REOPENED => 'Переоткрыт',
        self::STATUS_IN_PROGRESS => 'В работе',
        self::STATUS_RESOLVED => 'Решённый',
        self::STATUS_VERIFIED => 'Проверен',
        self::STATUS_CLOSED => 'Закрыт',
    ];

    const TIME_INTERVAL_HOUR = 'hour';
    const TIME_INTERVAL_DAY = 'day';
    const TIME_INTERVAL_WEEK = 'week';
    const TIME_INTERVAL_MONTH = 'month';

    static $timeIntervalAliases = [
        self::TIME_INTERVAL_HOUR => 'Час',
        self::TIME_INTERVAL_DAY => 'День',
        self::TIME_INTERVAL_WEEK => 'Неделя',
        self::TIME_INTERVAL_MONTH => 'Месяц',
    ];

    /**
     * @var string helper attribute to work with tags
     */
    public $tagNames;

    public static function getStatuses($status = null)
    {
        $statuses = [
            self::STATUS_NEW => Yii::t('task', 'New'),
            self::STATUS_OPENED => Yii::t('task', 'Opened'),
            self::STATUS_REOPENED => Yii::t('task', 'Reopened'),
            self::STATUS_IN_PROGRESS => Yii::t('task', 'In progress'),
            self::STATUS_RESOLVED => Yii::t('task', 'Resolved'),
            self::STATUS_VERIFIED => Yii::t('task', 'Verified'),
            self::STATUS_CLOSED => Yii::t('task', 'Closed'),
        ];

        if (is_null($status)) {
            return $statuses;
        } else {
            return isset($statuses[$status]) ? $statuses[$status] : null;
        }
    }

    public function behaviors()
    {
        return [
            // for different configurations, please see the code
            // we have created tables and relationship in order to
            // use defaults settings
            Taggable::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'task';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description', 'deadlineTextPart', 'status'], 'string'],
            [['price', 'negotiatedPrice', 'deadlineNumPart', 'authorId', 'performerId'], 'integer'],
            [['title', 'deadlineNumPart', 'deadlineTextPart'], 'required'],
            [['createdDate', 'lastUpdatedDate', 'tagNames'], 'safe'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Заголовок',
            'description' => 'Описание',
            'price' => 'Цена',
            'negotiatedPrice' => 'Договорная',
            'deadlineNumPart' => 'Кол-во',
            'deadlineTextPart' => 'Период',
            'authorId' => 'Заказчик',
            'performerId' => 'Испольнитель',
            'status' => 'Статус',
            'createdDate' => 'Дата добавления',
            'lastUpdatedDate' => 'Последее изменение',
            'tagNames' => 'Теги',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPerformer()
    {
        return $this->hasOne(User::className(), ['id' => 'performerId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'authorId']);
    }

    public function getStatusAliases($userType = null, $useCurrentStatus = true)
    {
        $statuses = self::getStatuses();
        if (!is_null($userType)) {
            switch ($userType) {
                case User::TYPE_CUSTOMER:
                    $statuses = [
                        self::STATUS_NEW => Yii::t('task', 'New'),
                        self::STATUS_REOPENED => Yii::t('task', 'Reopened'),
                        self::STATUS_VERIFIED => Yii::t('task', 'Verified'),
                        self::STATUS_CLOSED => Yii::t('task', 'Closed'),
                    ];
                    if ($useCurrentStatus) {
                        switch ($this->status) {
                            case self::STATUS_REOPENED:
                            case self::STATUS_VERIFIED:
                                unset($statuses[self::STATUS_NEW]);
                                break;
                            case self::STATUS_RESOLVED:
                                $statuses = [
                                    self::STATUS_REOPENED => Yii::t('task', 'Reopened'),
                                    self::STATUS_RESOLVED => Yii::t('task', 'Resolved'),
                                    self::STATUS_VERIFIED => Yii::t('task', 'Verified'),
                                    self::STATUS_CLOSED => Yii::t('task', 'Closed'),
                                ];
                                break;
                            case self::STATUS_CLOSED:
                                $statuses = [
//                                    self::STATUS_REOPENED => Yii::t('task', 'Reopened'),
                                    self::STATUS_CLOSED => Yii::t('task', 'Closed'),
                                ];
                                break;
                        }
                    }
                    break;
                case User::TYPE_PERFORMER:
                    $statuses = [
                        self::STATUS_NEW => Yii::t('task', 'New'),
                        self::STATUS_OPENED => Yii::t('task', 'Opened'),
                        self::STATUS_REOPENED => Yii::t('task', 'Reopened'),
                        self::STATUS_IN_PROGRESS => Yii::t('task', 'In progress'),
                        self::STATUS_RESOLVED => Yii::t('task', 'Resolved'),
                    ];
                    if ($useCurrentStatus) {
                        switch ($this->status) {
                            case self::STATUS_NEW:
                                $statuses = [
                                    self::STATUS_NEW => Yii::t('task', 'New'),
                                    self::STATUS_OPENED => Yii::t('task', 'Opened'),
                                ];
                                break;
                            case self::STATUS_OPENED:
                                unset($statuses[self::STATUS_NEW]);
                                unset($statuses[self::STATUS_REOPENED]);
                                break;
                            case self::STATUS_IN_PROGRESS:
                                unset($statuses[self::STATUS_NEW]);
                                unset($statuses[self::STATUS_OPENED]);
                                unset($statuses[self::STATUS_REOPENED]);
                                break;
                            case self::STATUS_REOPENED:
                                unset($statuses[self::STATUS_NEW]);
                                unset($statuses[self::STATUS_OPENED]);
                                break;
                            case self::STATUS_RESOLVED:
                                $statuses = [
                                    self::STATUS_REOPENED => Yii::t('task', 'Reopened'),
                                    self::STATUS_RESOLVED => Yii::t('task', 'Resolved'),
                                ];
                                break;
                            case self::STATUS_CLOSED:
                                $statuses = [
                                    self::STATUS_CLOSED => Yii::t('task', 'Closed'),
                                ];
                                break;
                        }
                    }
                    break;
            }
        }

        return $statuses;
    }

    public function getStatusAlias()
    {
        return !is_null($this->status) ? self::getStatuses($this->status) : null;
    }

    /**
     * @param null $timeInterval
     * @return array
     */
    public static function getTimeIntervalAliases($timeInterval = null)
    {
        return is_null($timeInterval) ? self::$timeIntervalAliases : self::$timeIntervalAliases[$timeInterval];
    }

    public function getNegotiatedPriceValueAlias()
    {
        return $this->negotiatedPrice ? 'Да' : 'Нет';
    }

    public static function getBoolAliases()
    {
        return ['Да', 'Нет'];
    }

    public function getDeadlineTextPartAlias()
    {
        return self::$timeIntervalAliases[$this->deadlineTextPart];
    }

    public function getFancy($attribute)
    {
        $result = '';

        switch ($attribute) {
            case 'price':
                $result = Yii::$app->formatter->asNumber($this->price) . ' руб.';
                break;
            case 'createdDate':
                $result = Yii::$app->formatter->asDate($this->createdDate, 'short');
                break;
            case 'deadline':
                $result = \Yii::t('app', '{n} ' . $this->deadlineTextPart, ['n' => $this->deadlineNumPart]);
                break;
            case 'tagNames':
                $result = [];
                foreach ($this->getTagNames() as $eachTagName) {
                    $result[] = '<span class="label label-info">' . $eachTagName . '</span>';
                }
                $result = join(' ', $result);
                break;
            case 'viewCount':
                $result = \Yii::t('app', '{n} viewCount', ['n' => $this->viewCount]);
                break;
            case 'uniqueViewCount':
                $result = \Yii::t('app', '{n} viewCount', ['n' => $this->uniqueViewCount]);
                break;
            case 'responseCount':
                $result = \Yii::t('app', '{n} responseCount', ['n' => $this->responseCount]);
                break;
        }
        return $result;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTags()
    {
        return $this->hasMany(Tag::className(), ['id' => 'tag_id'])->viaTable('tbl_task_tag_assn', ['task_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttachments()
    {
        return $this->hasMany(TaskAttachment::className(), ['taskId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategories()
    {
        return $this->hasMany(TaskCategory::className(), ['id' => 'categoryId'])->viaTable('task_category_assn', ['taskId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategoriesIDs()
    {
        $categoriesIDs = [];

        $rows = $this->getCategories()
            ->select('id')
            ->asArray()
            ->all();

        foreach ($rows as $eachRow) {
            $categoriesIDs[$eachRow['id']] = 1;
        }

        return $categoriesIDs;
    }

    /**
     * @return array
     */
    public function getTagNames()
    {
        if (!is_array($this->tagNames)) {
            $this->tagNames = [];
            foreach ($this->getTags()->asArray()->all() as $eachTag) {
                $this->tagNames[] = $eachTag['name'];
            }
        }
        return $this->tagNames;
    }

    public function hasTags()
    {
        return count($this->getTagNames()) > 0;
    }

    public function getViewUrl()
    {
        return Yii::$app->urlManager->createAbsoluteUrl(['/task/view', 'id' => $this->id]);
    }

    /*public function getResponseCount()
    {
        return (new Query())
            ->from(self::tableName() . ' t')
            ->innerJoin(PerformerTaskRespond::tableName() . ' pts', 't.id = pts.taskId')
            ->where('t.id = :id', [':id' => $this->id])
            ->count('pts.id');
    }*/

    /**
     * @param $categoryIDs
     */
    public function saveCategoriesIDs($categoryIDs)
    {
        // удаляем, сохраняем категории
        if (!$this->isNewRecord) {
            Yii::$app->db->createCommand()->delete(TaskCategoryAssn::tableName(), 'taskId = :taskId', ['taskId' => $this->id])->execute();
        }
        foreach ($categoryIDs as $eachCatId => $v) {
            if (!$v) { // категория не выбрана
                continue;
            }
            Yii::$app->db->createCommand()->insert(TaskCategoryAssn::tableName(), ['taskId' => $this->id, 'categoryId' => $eachCatId])->execute();
        }
    }

    /**
     * @return string
     */
    public function getCssClassByStatus()
    {
        switch ($this->status) {
            case self::STATUS_NEW:
                $cssClass = 'primary';
                break;
            case self::STATUS_OPENED:
                $cssClass = 'warning';
                break;
            case self::STATUS_REOPENED:
                $cssClass = 'primary';
                break;
            case self::STATUS_IN_PROGRESS:
                $cssClass = 'info';
                break;
            case self::STATUS_RESOLVED:
                $cssClass = 'success';
                break;
            case self::STATUS_VERIFIED:
                $cssClass = 'default';
                break;
            case self::STATUS_CLOSED:
                $cssClass = 'default';
                break;
            default:
                $cssClass = 'primary';

        }
        return $cssClass;
    }
}
