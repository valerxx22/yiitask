<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "performer_task_respond".
 *
 * @property integer $taskId
 * @property integer $performerId
 * @property integer $status
 * @property string $createdDate
 *
 * @property User $performer
 * @property Task $task
 */
class PerformerTaskRespond extends \yii\db\ActiveRecord
{
    const STATUS_NEW = 0;
    const STATUS_VIEWED = 1;
    const STATUS_ACCEPTED = 2;
    const STATUS_REFUSED = 3;

    public static $statuses = [
        self::STATUS_NEW => 'Новый',
        self::STATUS_VIEWED => 'Просмотрен',
        self::STATUS_ACCEPTED => 'Да',
        self::STATUS_REFUSED => 'Отказ',
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'performer_task_respond';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['taskId', 'performerId'], 'required'],
            [['taskId', 'performerId', 'status'], 'integer'],
            [['status'],  'in', 'range' => array_keys(self::$statuses)],
            [['createdDate'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'taskId' => Yii::t('app', 'Task ID'),
            'performerId' => Yii::t('app', 'Performer ID'),
            'status' => Yii::t('app', 'Status'),
            'createdDate' => Yii::t('app', 'Created Date'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPerformer()
    {
        return $this->hasOne(User::className(), ['id' => 'performerId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTask()
    {
        return $this->hasOne(Task::className(), ['id' => 'taskId']);
    }
}
