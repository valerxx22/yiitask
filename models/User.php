<?php

namespace app\models;

use app\helpers\events\TaskAssign;
use app\helpers\events\TaskStatusChanged;
use Yii;
use yii\base\Object;
use yii\db\Query;
use yii\helpers\Security;
use yii\web\NotFoundHttpException;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $email
 * @property string $username
 * @property string $phone
 * @property string $firstName
 * @property string $lastName
 * @property string $patronymic
 * @property string $type
 * @property string $passwordHash
 * @property string $authKey
 * @property string $accessToken
 * @property string $createdDate
 *
 * @property Task[] $tasks
 */
class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    const TYPE_CUSTOMER = 'customer';
    const TYPE_PERFORMER = 'performer';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type'], 'string'],
            [['email', 'username', 'firstName', 'lastName', 'type'], 'required'],
            [['createdDate'], 'safe'],
            [['email'], 'email'],
            [['email', 'username'], 'unique'],
            [['firstName', 'lastName', 'patronymic', 'username'], 'string', 'max' => 25],
            [['passwordHash', 'authKey', 'accessToken'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email' => 'E-mail',
            'username' => 'Ник',
            'phone' => 'Телефон',
            'firstName' => 'Имя',
            'lastName' => 'Фамилия',
            'patronymic' => 'Отчество',
            'type' => 'Тип юзера',
            'passwordHash' => Yii::t('app', 'Password Hash'),
            'createdDate' => 'Дата регистрации',
        ];
    }

    public function init()
    {
        parent::init();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTasks()
    {
        return $this->hasMany(Task::className(), ['authorId' => 'id']);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return self::find()->where('id = :id', ['id' => $id])->one();
    }

    /**
     * @param $email
     * @return User|null
     */
    public static function findByEmail($email)
    {
        return self::find()->where('email = :email', ['email' => $email])->one();
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token)
    {
        return self::find()->where('accessToken = :accessToken', ['accessToken' => $token])->one();
    }

    public function encryptPassword()
    {
        $this->passwordHash = Security::generatePasswordHash($this->passwordHash);
    }

    /**
     * Validates password
     *
     * @param  string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Security::validatePassword($password, $this->passwordHash);
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * @param null $type
     * @return array
     */
    public static function getTypeAliases($type = null)
    {
        $types = [
            self::TYPE_CUSTOMER => Yii::t('app', 'Customer'),
            self::TYPE_PERFORMER => Yii::t('app', 'Performer'),
        ];

        return is_null($type) ? $types : $types[$type];
    }

    public function getFullName()
    {
        return trim($this->firstName . ' ' . $this->patronymic . ' ' . $this->lastName);
    }

    public static function getCustomers()
    {
        return self::find()->where('type = :type', ['type' => self::TYPE_CUSTOMER])->all();
    }

    public static function getPerformers()
    {
        return self::find()->where('type = :type', ['type' => self::TYPE_PERFORMER])->all();
    }

    public function getIsCustomer()
    {
        return $this->type === self::TYPE_CUSTOMER;
    }

    public function getIsPerformer()
    {
        return $this->type === self::TYPE_PERFORMER;
    }

    public function getReallyFullName()
    {
        return trim($this->firstName . ' ' . $this->lastName . ' (' . $this->username . ')');
    }

    /**
     * @param $taskId
     * @return \stdClass
     */

    /**
     * @param $taskId
     * @return \stdClass Свойства: canRespond, errorMessage, errorCode
     * @throws \yii\web\NotFoundHttpException
     */
    public function getCanRespondTask($taskId)
    {
        $result = new \stdClass();
        $result->canRespond = false;
        $result->errorMessage = '';
        $result->errorCode = 0;

        $task = Task::findOne(['id' => $taskId]);
        if (!$task) {
            throw new NotFoundHttpException();
        }

        if ($task->performerId == $this->id) {
            $result->errorMessage = Yii::t('app', 'You already are performer');
        } elseif ($taskRespond = $this->getTaskRespond($taskId)) {

            switch ($taskRespond->status) {
                case PerformerTaskRespond::STATUS_ACCEPTED:
                    $result->errorMessage = Yii::t('app', 'Your response was accepted');
                    $result->errorCode = 0;
                    break;
                case PerformerTaskRespond::STATUS_NEW:
                    $result->errorMessage = Yii::t('app', 'You already responded to this task');
                    $result->errorCode = 1;
                    break;
                case PerformerTaskRespond::STATUS_VIEWED:
                    $result->errorMessage = Yii::t('app', 'Your response was viewed');
                    $result->errorCode = 3;
                    break;
                case PerformerTaskRespond::STATUS_REFUSED:
                    $result->errorMessage = Yii::t('app', 'Your response was refused');
                    $result->errorCode = 4;
                    break;
            }
        } else {
            $result->canRespond = true;
        }

        return $result;
    }

    /**
     * @param $taskId
     * @return bool
     */
    public function getRespondedToTask($taskId)
    {
        $result = PerformerTaskRespond::find()->where(['taskId' => $taskId, 'performerId' => $this->id])->count();
        return $result > 0;
    }

    /**
     * @param $taskId
     * @return PerformerTaskRespond|null
     */
    public function getTaskRespond($taskId)
    {
        return PerformerTaskRespond::find()->where(['performerId' => $this->id, 'taskId' => $taskId])->one();
    }

    public function getResponsesInfo()
    {
        $info = [];

        foreach (PerformerTaskRespond::$statuses as $eachStatusId => $eachStatusName) {
            $info[$eachStatusId] = [
                'id' => $eachStatusId,
                'name' => $eachStatusName,
                'count' => 0,
            ];
        }

        $data = (new Query())
            ->select('COUNT(pts.status) as `count`, pts.status')
            ->from(Task::tableName() . ' t')
            ->innerJoin(PerformerTaskRespond::tableName() . ' pts', 't.id = pts.taskId')
            ->where('t.authorId = :authorId', [':authorId' => $this->id])
            ->groupBy('pts.status')
            ->all();

        foreach ($data as $eachRow) {
            $info[$eachRow['status']]['count'] = $eachRow['count'];
        }

        return $info;
    }

    public function getProfileUrl()
    {
        $route = '/' . ($this->getIsPerformer() ? 'performer' : 'customer') . '/default/view';
        return Yii::$app->urlManager->createAbsoluteUrl([$route, 'id' => $this->id]);
    }

    /**
     * @param TaskAssign $event
     */
    public function assignTaskToPerformer(TaskAssign $event)
    {
        \app\helpers\User::sendEmail(
            Yii::t('email', 'New task'),
            $event->getPerformer()->email,
            'user/newTask',
            [
                'performer' => $event->getPerformer(),
                'customer' => $event->getCustomer(),
                'task' => $event->getTask()
            ]
        );
    }

    public function taskStatusChanged(TaskStatusChanged $event)
    {
        // todo определить как и куда отсылать уведомления

        \app\helpers\User::sendEmail(
            Yii::t('email', 'Task status changed'),
            $event->getTask()->author->email,
            'common/taskStatusChanged',
            [
                'user' => $event->getUser(),
                'task' => $event->getTask(),
                'oldStatus' => $event->getOldStatus(),
                'newStatus' => $event->getNewStatus(),
            ]
        );

        if ($event->getTask()->performer) {
            \app\helpers\User::sendEmail(
                Yii::t('email', 'Task status changed'),
                $event->getTask()->performer->email,
                'common/taskStatusChanged',
                [
                    'user' => $event->getUser(),
                    'task' => $event->getTask(),
                    'oldStatus' => $event->getOldStatus(),
                    'newStatus' => $event->getNewStatus(),
                ]
            );
        }
    }
}