<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Task;

/**
 * TaskSearch represents the model behind the search form about `app\models\Task`.
 */
class TaskSearch extends Task
{
    public $categoryIDs = [];

    public function rules()
    {
        return [
            [['id', 'price', 'negotiatedPrice', 'deadlineNumPart', 'authorId', 'performerId'], 'integer'],
            [['title', 'description', 'deadlineTextPart', 'status', 'createdDate', 'lastUpdatedDate', 'categoryIDs'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Task::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $categoryIDs = [];
        foreach ($this->categoryIDs as $eachCatId => $eachCatExist) {
            if ($eachCatExist) {
                $categoryIDs[] = $eachCatId;
            }
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'price' => $this->price,
            'negotiatedPrice' => $this->negotiatedPrice,
            'deadlineNumPart' => $this->deadlineNumPart,
            'authorId' => $this->authorId,
            'performerId' => $this->performerId,
            'createdDate' => $this->createdDate,
            'lastUpdatedDate' => $this->lastUpdatedDate,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'deadlineTextPart', $this->deadlineTextPart])
            ->andFilterWhere(['like', 'status', $this->status]);

        if (count($categoryIDs) > 0) {
            $query->joinWith('categories');
            $query->andFilterWhere(['in', 'task_category.id', $categoryIDs]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $dataProvider;
    }
}
