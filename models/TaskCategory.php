<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "task_category".
 *
 * @property integer $id
 * @property integer $nameRu
 * @property integer $nameEn
 * @property integer $active
 * @property string $createdDate
 *
 * @property TaskCategoryAssn[] $taskCategoryAssns
 * @property TaskCategory[] $children
 */
class TaskCategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'task_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nameRu', 'nameEn', 'createdDate'], 'required'],
            [['nameRu', 'nameEn', 'active'], 'integer'],
            [['createdDate'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'nameRu' => Yii::t('app', 'Name Ru'),
            'nameEn' => Yii::t('app', 'Name En'),
            'active' => Yii::t('app', 'Active'),
            'createdDate' => Yii::t('app', 'Created Date'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaskCategoryAssns()
    {
        return $this->hasMany(TaskCategoryAssn::className(), ['categoryId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChildren()
    {
        return $this->hasMany(self::className(), ['parentId' => 'id']);
    }
}
