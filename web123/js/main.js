/**
 * Created by itsurka on 27.06.14.
 */

$(function() {

    $('[name="reset-performer"]').click(function() {

        $('#taskform-performername, #taskform-performerid').val('');
        $('#taskform-performername').focus();
    });

    /*$('.btn[data-loading-text]').click(function() {
     $(this).button('loading');
     });*/

    $('#taskform-performername').autocomplete({
        'source': '/customer/performer/search',
        'minLength': 3,
        'select': function(a, b) {
            $('#taskform-performerid').val(b.item.id);
        }
    });

    $(".input-money").maskMoney({thousands: ',', precision: 0}).maskMoney('mask');
    $(".input-time-period").mask("9?9");

    // search tasks form
    $('#tasksearch-title').keyup(function() {
        submitSearchForm();
    });
    $('.task-categories-tree.js-search-form').mouseup(function() {
        submitSearchForm();
    });


});

submitSearchFormTimer = false;

/**
 * todo отладить
 * @param auto
 * @returns {boolean}
 */
function submitSearchForm(auto) {

    if (auto === undefined) {
        auto = false;
    }

    console.log('submitSearchFormTimer', submitSearchFormTimer);

    if (auto) {
//        $('#search-task-form').submit();
    } else {
        submitSearchFormTimer = setTimeout(function() {
            submitSearchForm(true);
        }, 5000);
    }
    return true;
}