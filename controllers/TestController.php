<?php

namespace app\controllers;

use app\models\forms\PerformerForm;
use app\models\User;
use Yii;
use app\models\Task;
use app\models\search\TaskSearch;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotAcceptableHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TaskController implements the CRUD actions for Task model.
 */
class TestController extends FrontController
{
    public function behaviors()
    {
        return [];
    }

    public function actionComments()
    {
        return $this->render('comments');
    }

    public function actionEcho()
    {
        echo '$var[]<pre>';
        print_r(Yii::$aliases);
        echo '</pre>';
    }
}
