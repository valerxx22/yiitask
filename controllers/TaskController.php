<?php

namespace app\controllers;

use app\models\forms\PerformerForm;
use app\models\forms\TaskRespondForm;
use app\models\PerformerTaskRespond;
use app\models\User;
use Yii;
use app\models\Task;
use app\models\search\TaskSearch;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotAcceptableHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TaskController implements the CRUD actions for Task model.
 */
class TaskController extends FrontController
{
    /**
     * Страница поиска задач исполнителем
     *
     * @return string
     */
    public function actionSearch()
    {
        $searchModel = new TaskSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->post());

        return $this->render('search', [
            'model' => new Task(),
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    public function actionView($id)
    {
        $request = Yii::$app->request;
        $task = $this->findModel($id);
        $taskRespondForm = new TaskRespondForm();
        $taskRespondForm->taskId = $task->id;

        if ($this->user && $this->user->getIsPerformer()) {
            $taskRespondForm->userId = $this->user->id;
        }

        // todo перенести код в event onView
        // просмотры
        $viewedTasks = Yii::$app->session->get('viewedTasks', []);
        if (!isset($viewedTasks[$task->id])) {
            $viewedTasks[$task->id] = time();
            Yii::$app->session->set('viewedTasks', $viewedTasks);

            $task->viewCount += 1;
            $task->uniqueViewCount += 1;
            $task->update(false);
        }

        if ($request->isPost) {
            // обработка отклика на задачу
            if ($formData = $request->post('TaskRespondForm')) {
                if ($taskRespondForm->load($request->post()) && $taskRespondForm->validate()) {
                    $performerTaskRespond = new PerformerTaskRespond();
                    $performerTaskRespond->setAttributes([
                        'taskId' => $taskRespondForm->taskId,
                        'performerId' => $taskRespondForm->userId,
                    ]);
                    $performerTaskRespond->insert();

                    $task->responseCount += 1;
                    $task->update(false);

                    // todo event - new respond created

                    Yii::$app->session->setFlash('respond_created_msg', Yii::t('app', 'New respond successfully created!'));
                    $this->refresh();
                }
            }
        }

        return $this->render('view', [
            'task' => $task,
            'taskRespondForm' => $taskRespondForm,
        ]);
    }

    /**
     * Finds the Task model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Task the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Task::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
