<?php

namespace app\controllers;

use app\models\User;
use Yii;
use yii\web\Controller;

class FrontController extends Controller
{
    public $layout = '@app/views/layouts/main.php';

    /** @var User|null */
    public $user;

    public function init()
    {
        parent::init();

        if (!\Yii::$app->user->isGuest) {
            $this->user = \Yii::$app->user->identity;
        }

        $this->on('taskStatusChanged', ['app\models\User', 'taskStatusChanged']);
    }
}
