<?php

namespace app\controllers;

use app\models\forms\PerformerForm;
use app\models\Tag;
use app\models\User;
use Yii;
use app\models\Task;
use app\models\search\TaskSearch;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotAcceptableHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

class TagController extends FrontController
{
    // On TagController (example)
    // actionList to return matched tags
    public function actionList($query)
    {
        ini_set('display_errors', 'On');
        error_reporting(E_ALL);

        $models = Tag::find()
            ->where(['like', 'name', $query])
            ->asArray()
            ->limit(7)
            ->all();

        $items = [];

        /*foreach ($models as $model) {
            $items[] = ['name' => $model->name];
        }*/

        foreach ($models as $model) {
            $items[] = ['name' => $model['name']];
        }
        // We know we can use ContentNegotiator filter
        // this way is easier to show you here :)
        Yii::$app->response->format = Response::FORMAT_JSON;

        return $items;
    }
}
