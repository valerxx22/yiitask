<?php
/**
 * FaxService.php
 *
 * @author Igor Tsurka <turcaigor@gmail.com>
 * @date 01.07.14
 */

namespace components;


class FaxService
{
    const APPEND_FAX_URL = 'http://192.168.1.4/faxservice/index.php';

    private $_fax;
    private $_faxId;
    private $_pdfFilename;
    private $_maxRetries;
    private $_callbackUrl;
    private $_hasError = false;
    private $_errors = [];

    public function __construct($fax, $faxId, $pdfFilename, $callbackUrl, $maxRetries = 3)
    {
        $this->_fax = $fax;
        $this->_faxId = $faxId;
        $this->_pdfFilename = $pdfFilename;
        $this->_callbackUrl = $callbackUrl;
        $this->_maxRetries = $maxRetries;
    }

    public function send()
    {
        if (!$this->validate()) {
            return false;
        }

        try {

            $post = [
                'fax' => $this->_fax,
                'faxId' => $this->_faxId,
                'maxRetries' => $this->_maxRetries,
                'returnUrl' => $this->_callbackUrl,
                'file' => new \CURLFile($this->_pdfFilename, 'application/pdf', $this->_faxId . '.pdf')
            ];

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, self::APPEND_FAX_URL);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $response = json_decode(curl_exec($ch), true);
            curl_close($ch);

            if ($response && $response['status'] == 'success') {
                return true;
            } else {
                $this->_errors['send'] = 'Ошибка при отправке факса';
            }
        } catch (\Exception $e) {
            $this->_errors['send'] = 'Ошибка при отправке запроса на сервер: "' . $e->getMessage() . '"';
        }

        return false;
    }

    public function validate($clearErrors = true)
    {
        if ($clearErrors) {
            $this->_errors = [];
            $this->_hasError = false;
        }

        if (empty($this->_fax)) {
            $this->_errors['fax'] = 'Неверный номер факса';
        }

        if (!is_numeric($this->_faxId) || $this->_faxId < 1) {
            $this->_errors['faxId'] = 'Неверный ID факса';
        }

        if (empty($this->_pdfFilename)) {
            $this->_errors['pdfFilename'] = 'PDF файл не указан';
        } elseif (strtolower(pathinfo($this->_pdfFilename, PATHINFO_EXTENSION)) != 'pdf') {
            $this->_errors['pdfFilename'] = 'Расширение файла должно быть "PDF"';
        } elseif (!file_exists($this->_pdfFilename)) {
            $this->_errors['pdfFilename'] = 'PDF файл не найден';
        }

        if (filter_var($this->_callbackUrl, FILTER_VALIDATE_URL) === false) {
            $this->_errors['callbackUrl'] = 'Неверный callback адрес';
        }

        if (!is_numeric($this->_maxRetries) || $this->_maxRetries < 1) {
            $this->_errors['maxRetries'] = 'Неверное число кол-ва попыток отправки';
        }

        if (count($this->_errors) > 0) {
            $this->_hasError = true;
        }

        return !$this->_hasError;
    }

    public function getHasError()
    {
        return $this->_hasError;
    }

    public function getErrors()
    {
        return $this->_errors;
    }
}