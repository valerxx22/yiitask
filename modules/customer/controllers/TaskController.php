<?php

namespace app\modules\customer\controllers;

use app\controllers\FrontController;
use app\helpers\events\TaskAssign;
use app\helpers\events\TaskStatusChanged;
use app\models\forms\PerformerForm;
use app\models\forms\StatusForm;
use app\models\forms\TaskForm;
use app\models\forms\TaskPriceForm;
use app\models\TaskCategory;
use app\models\TaskCategoryAssn;
use app\models\User;
use Yii;
use app\models\Task;
use app\models\search\TaskSearch;
use yii\db\Expression;
use yii\db\Query;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotAcceptableHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

class TaskController extends FrontController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new TaskSearch;
        Yii::$app->request->setQueryParams(['TaskSearch' => ['authorId' => $this->user->id]]);
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'model' => new Task(),
        ]);
    }

    /**
     * Страница просмотра/редактирования задачи
     *
     * @param $id
     * @return string
     */
    public function actionView($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $performerForm = new PerformerForm();

        $taskPriceForm = new TaskPriceForm();
        $taskPriceForm->price = $model->price;
        $taskPriceForm->deadlineNumPart = $model->deadlineNumPart;
        $taskPriceForm->deadlineTextPart = $model->deadlineTextPart;

        $statusForm = new StatusForm();
        $statusForm->status = $model->status;
        $statusForm->statusAlias = $model->getStatusAlias();

        $taskForm = new TaskForm();
        $taskForm->title = $model->title;
        $taskForm->description = $model->description;

        if ($model->performer) {
            $performerForm->performerName = $model->performer->getReallyFullName();
            $performerForm->performerId = $model->performer->id;
        }

        if ($request->isPost) {
            // обработка исполнителя
            if ($request->post('performer_form')) {
                $performerForm->attributes = $request->post('PerformerForm');

                if ($performerForm->validate()) {
                    $model->performerId = $performerForm->performerId;
                    $model->save(false);
                }
            }

            // обработка статуса
            if ($request->post('status_form')) {
                $statusForm->attributes = $request->post('StatusForm');

                if ($statusForm->validate()) {
                    $model->status = $statusForm->status;
                    $model->save(false);
                }
            }

            // обработка цены и сроков
            if ($request->post('task_price_form')) {
                $taskPriceForm->attributes = $request->post('TaskPriceForm');
                $taskPriceForm->price = preg_replace("/[^0-9]/", "", $request->post('TaskPriceForm')['price']);

                if ($taskPriceForm->validate()) {
                    $model->price = $taskPriceForm->price;
                    $model->deadlineNumPart = $taskPriceForm->deadlineNumPart;
                    $model->deadlineTextPart = $taskPriceForm->deadlineTextPart;
                    $model->save(false);
                }
            }

            // обработка текстов задачи
            if ($request->post('task_form')) {
                $taskForm->attributes = $request->post('TaskForm');

                if ($taskForm->validate()) {
                    $model->title = $taskForm->title;
                    $model->description = $taskForm->description;
                    $model->save(false);
                }
            }

            $model->lastUpdatedDate = new Expression('NOW()');
            $model->save(false);

            $this->refresh();
        }

        return $this->render('view', [
            'model' => $model,
            'taskForm' => $taskForm,
            'performerForm' => $performerForm,
            'statusForm' => $statusForm,
            'taskPriceForm' => $taskPriceForm,
        ]);
    }

    public function actionCreate()
    {
        /*if (!\Yii::$app->user->can('createTask')) {
            throw new ForbiddenHttpException();
        }*/

        $model = new Task;
        $model->authorId = \Yii::$app->user->id;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     *  Добавление/редактирование задачи
     *
     * @param null $id
     * @return string
     */
    public function actionUpdate($id = null)
    {
        if (is_null($id)) {
            $model = new Task();
            $model->authorId = $this->user->id;
        } else {
            $model = $this->findModel($id);
        }
        $oldPerformerId = $model->performerId;
        $oldStatus = $model->status;

        $request = Yii::$app->request;

        $taskForm = new TaskForm();
        $taskForm->attributes = $model->attributes;
        $taskForm->tagNames = $model->tagNames;

        if ($model->performer) {
            $taskForm->performerName = $model->performer->getReallyFullName();
            $taskForm->performerId = $model->performer->id;
        }

        /** @var TaskCategory $taskCategories */
        $taskCategories = TaskCategory::find()->where([
            'active' => 1,
            'parentId' => 0,
        ])->all();

        if (!$model->isNewRecord) {
            $taskForm->categoryIDs = $model->getCategoriesIDs();

            foreach ($taskCategories as $eachCategory) {
                $taskForm->categoryIDs[$eachCategory->id] = isset($taskForm->categoryIDs[$eachCategory->id]) ? 1 : 0;
            }
        }

        if ($request->isPost) {

            $this->on('assignTaskToPerformer', ['app\models\User', 'assignTaskToPerformer']);
            $refresh = false;

            $taskForm->attributes = $request->post('TaskForm');

            // обработка категорий
            if ($request->post('categories_form')) {
                if ($taskForm->validate(['categoryIDs'])) {
                    $model->saveCategoriesIDs($taskForm->categoryIDs);
                    $model->lastUpdatedDate = new Expression('NOW()');
                    $model->save();
                    $refresh = true;
                }
            }

            // обработка исполнителя
            if ($request->post('performer_form')) {
                if ($taskForm->validate(['performerId'])) {
                    $model->performerId = $taskForm->performerId;
                    $model->lastUpdatedDate = new Expression('NOW()');
                    $model->save();
                    $model->refresh();
                    $refresh = true;
                }
            }

            // обработка статуса
            if ($request->post('status_form')) {
                if ($taskForm->validate(['status'])) {
                    $model->status = $taskForm->status;
                    $model->lastUpdatedDate = new Expression('NOW()');
                    $model->save();
                    $model->refresh();
                    $refresh = true;
                }
            }

            // обработка цены и сроков
            if ($request->post('task_price_form')) {

                if ($taskForm->validate(['price', 'deadlineNumPart', 'deadlineTextPart'])) {
                    $model->price = $taskForm->price;
                    $model->deadlineNumPart = $taskForm->deadlineNumPart;
                    $model->deadlineTextPart = $taskForm->deadlineTextPart;
                    $model->lastUpdatedDate = new Expression('NOW()');
                    $model->save();
                    $refresh = true;
                }
            }

            // обработка текстов задачи
            if ($request->post('task_form')) {

                $validateAttributes = [
                    'title',
                    'description',
                    'status',
                    'price',
                    'deadlineNumPart',
                    'deadlineTextPart',
                    'tagNames',
                    'categoryIDs',
                ];

                if (!$model->isNewRecord) {
                    $validateAttributes = [
                        'title',
                        'description',
                        'tagNames',
                    ];
                }

                if ($taskForm->validate($validateAttributes)) {
                    $model->attributes = $taskForm->getAttributes($validateAttributes);
                    $model->lastUpdatedDate = new Expression('NOW()');
                    $model->save();
                    $model->refresh();
                    $model->saveCategoriesIDs($taskForm->categoryIDs);
                    $refresh = true;
                }
            }

            // эвент - задача назначена исполнителю
            if ($model->performerId && $oldPerformerId != $model->performerId) {
                $event = (new TaskAssign())
                    ->setCustomer($this->user)
                    ->setPerformer($model->performer)
                    ->setTask($model);
                $this->trigger('assignTaskToPerformer', $event);
            }

            // эвент - задача сменила статус
            if ($oldStatus != $model->status) {
                $event = new TaskStatusChanged();
                $event->setTask($model);
                $event->setUser($this->user);
                $event->setOldStatus($oldStatus);
                $event->setNewStatus($model->status);
                $this->trigger('taskStatusChanged', $event);
            }

            if ($refresh) {
                $this->refresh();
            }
        }

        return $this->render('update', [
            'model' => $model,
            'taskForm' => $taskForm,
            'user' => $this->user,
        ]);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionDelete($id)
    {
        /*if (!\Yii::$app->user->can('deleteTask')) {
            throw new ForbiddenHttpException();
        }*/

        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionPerformersSearch()
    {
        exit('e1');
    }

    /**
     * Finds the Task model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Task the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Task::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
