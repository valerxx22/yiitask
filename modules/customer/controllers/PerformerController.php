<?php

namespace app\modules\customer\controllers;

use app\controllers\FrontController;
use app\models\forms\PerformerForm;
use app\models\User;
use Yii;
use app\models\Task;
use app\models\search\TaskSearch;
use yii\db\Query;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotAcceptableHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

class PerformerController extends FrontController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function actionSearch($term)
    {
        $query = new Query();
        $users = $query
            ->select(['u.id', 'u.username', 'u.firstname', 'u.lastname'])
            ->from(User::tableName(). ' u')
            ->where(['u.type' => User::TYPE_PERFORMER])
            ->andWhere('u.username LIKE :query OR u.firstname LIKE :query OR u.lastname LIKE :query', [':query' => '%' . $term . '%'])
            ->orderBy('u.id ASC')
            ->limit(10)
            ->all();

        $return = [];
        foreach ($users as $eachUser) {
            $value = trim($eachUser['firstname'] . ' ' . $eachUser['lastname']. ' (' . $eachUser['username'] . ')');
            $return[] = [
                'id' => $eachUser['id'],
                'value' => $value,
            ];
        }

        echo Json::encode($return);
        Yii::$app->end();
    }

    /**
     * Finds the Task model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Task the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Task::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
