<?php

namespace app\modules\customer;

class Customer extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\customer\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
