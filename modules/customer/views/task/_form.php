<?php

use app\models\forms\TaskForm;
use app\models\Task;
use app\models\TaskCategory;
use app\models\User;
use dosamigos\selectize\Selectize;
use itsurka\simpleComment\SimpleComment;
use mihaildev\ckeditor\CKEditor;
use app\widgets\TaskCategoriesTree;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Task $model
 * @var TaskForm $taskForm
 * @var User $user
 */


?>

<?php $form = ActiveForm::begin([
    'enableClientValidation' => false
]); ?>

    <?php echo $form->errorSummary([$taskForm]); ?>

    <div class="row show-grid">
        <div class="col-md-9">
            <div class="panel panel-primary">
                <div class="panel-heading"><?php echo Yii::t('app', 'Task'); ?></div>
                <div class="panel-body">
                    <table class="table">
                        <tbody>
                        <tr>
                            <th><?php echo $taskForm->getAttributeLabel('title'); ?></th>
                            <td><?php echo $form->field($taskForm, 'title', ['enableLabel' => false])->textInput(); ?></td>
                        </tr>
                        <tr>
                            <th><?php echo $model->getAttributeLabel('tagNames'); ?></th>
                            <td>
                                <?= $form->field($taskForm, 'tagNames', ['enableLabel' => false])->widget(Selectize::className(), [
                                    // calls an action that returns a JSON object with matched
                                    // tags
                                    'url' => ['/tag/list'],
                                    'options' => ['class' => 'form-control'],
                                    'clientOptions' => [
                                        'plugins' => ['remove_button'],
                                        'valueField' => 'name',
                                        'labelField' => 'name',
                                        'searchField' => ['name'],
                                        'create' => true,
                                    ],
                                ])->hint(Yii::t('app', 'Use commas to separate tags')); ?>
                            </td>
                        </tr>
                        <tr>
                            <th><?php echo $taskForm->getAttributeLabel('description'); ?></th>
                            <td>
                                <?php echo $form->field($taskForm, 'description', ['enableLabel' => false])->widget(CKEditor::className(), [
                                    'editorOptions' => [
                                        'preset' => 'standard', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
                                        'inline' => false, //по умолчанию false
                                    ],
                                ]); ?>
                            </td>
                        </tr>
                        <tr>
                            <th><?php echo $model->getAttributeLabel('createdDate'); ?></th>
                            <td><?php echo $model->createdDate; ?></td>
                        </tr>
                        <tr>
                            <th><?php echo $model->getAttributeLabel('attachments'); ?></th>
                            <td>
<!--                                --><?php //$attachmentForm = new \app\models\forms\TaskAttachmentForm(); ?>
<!--                                --><?php //echo $form->field($attachmentForm, 'attachment[0]', ['enableLabel' => false])->fileInput(); ?>

                                <?php echo $form->field($taskForm, 'attachments[0]')->fileInput(); ?>
                            </td>
                        </tr>
                        </tbody>
                    </table>

                    <?php echo Html::submitButton(Yii::t('app', 'Save'), [
                        'data-loading-text' => 'подождите...',
                        'class' => 'btn btn-primary btn-xs',
                        'name' => 'task_form',
                        'value' => 1
                    ]); ?>

                    <!--                    --><? //= DetailView::widget([
                    //                        'model' => $model,
                    //                        'attributes' => [
                    //                            'title',
                    //                            'description:ntext',
                    ////                            'price',
                    ////                            'negotiatedPrice',
                    ////                            'deadlineNumPart',
                    ////                            'deadlineTextPart',
                    ////                            'authorId',
                    ////                            'performerId',
                    ////                            'status',
                    //                            'createdDate',
                    //                            'lastUpdatedDate',
                    //                        ],
                    //                        'options' => ['class' => 'table']
                    //                    ])
                    ?>
                </div>
            </div>

            <div class="panel panel-primary">
                <div class="panel-heading"><?php echo Yii::t('app', 'Comments'); ?></div>
                <div class="panel-body">

                    <?php echo SimpleComment::widget([
                        'model' => $model,
                        'author' => $user,
                        'authorNameAttribute' => 'username',
                    ]); ?>

                </div>
            </div>

        </div>
        <div class="col-md-3">

            <div class="panel panel-primary">
                <div class="panel-heading"><?php echo Yii::t('app', 'Category'); ?></div>
                <div class="panel-body required <?php echo $taskForm->hasErrors('categoryIDs') ? 'has-error' : ''; ?>">
                    <?php echo TaskCategoriesTree::widget(['model' => $taskForm, 'attributeName' => 'categoryIDs']); ?>
                    <?php echo Html::error($taskForm, 'categoryIDs', ['tag' => 'span', 'class' => 'help-block text-center']); ?>

                    <?php if (!$model->isNewRecord): ?>
                        <?php echo Html::submitButton(Yii::t('app', 'Save'), [
                            'data-loading-text' => 'подождите...',
                            'class' => 'btn btn-primary btn-xs',
                            'name' => 'categories_form',
                            'value' => 1
                        ]); ?>
                    <?php endif; ?>
                </div>
            </div>

            <div class="panel panel-primary">
                <div class="panel-heading"><?php echo Yii::t('app', 'Performer'); ?></div>
                <div class="panel-body">

                    <?php /*echo $form->errorSummary([$taskForm, $taskForm, $taskForm]); */?>

                    <?php echo Html::activeHiddenInput($taskForm, 'performerId', ['class' => 'form-control']); ?>

                    <div class="form-group  field-performerform-performername required <?php echo $taskForm->hasErrors('performerId') ? 'has-error' : ''; ?>">
                        <div class="input-group">
                            <?php echo Html::activeTextInput($taskForm, 'performerName', ['class' => 'form-control jui-autocomplete']); ?>

                            <!--                            --><?php //echo AutoComplete::widget([
                            //                                'model' => $taskForm,
                            //                                'attribute' => 'performerName',
                            //                                'options' => [
                            //                                    'class' => 'form-control',
                            //                                    'placeholder' => 'введите имя',
                            //                                ],
                            //                                'clientOptions' => [
                            //                                    'source' => '/customer/performer/search',
                            //                                    'minLength' => 3,
                            //                                    'select' => new \yii\web\JsExpression('function(a, b){ $("#performerform-performerid").val(b.item.id); }'),
                            //                                ]
                            //                            ]);
                            ?>

                            <span class="input-group-btn">
                                    <button class="btn btn-primary" type="button" name="reset-performer">
                                        <span class="glyphicon glyphicon-remove"></span>
                                    </button>
                                </span>
                        </div>
                        <?php echo Html::error($taskForm, 'performerName', ['tag' => 'span', 'class' => 'help-block']); ?>
                    </div>

                    <?php if (!$model->isNewRecord): ?>
                        <?php echo Html::submitButton(Yii::t('app', 'Save'), [
                            'data-loading-text' => 'подождите...',
                            'class' => 'btn btn-primary btn-xs',
                            'name' => 'performer_form',
                            'value' => 1
                        ]); ?>
                    <?php endif; ?>

                </div>
            </div>

            <div class="panel panel-primary">
                <div class="panel-heading"><?php echo Yii::t('app', 'Status'); ?></div>
                <div class="panel-body">
                    <?php echo $form->field($taskForm, 'status', ['enableLabel' => false])->dropDownList($model->getStatusAliases(User::TYPE_CUSTOMER)); ?>

                    <?php if (!$model->isNewRecord): ?>
                        <?php echo Html::submitButton(Yii::t('app', 'Save'), [
                            'data-loading-text' => 'подождите...',
                            'class' => 'btn btn-primary btn-xs',
                            'name' => 'status_form',
                            'value' => 1
                        ]); ?>
                    <?php endif; ?>

                    <!--                    --><?php //Dropdown::begin(); ?>
                    <!--                    --><?php //echo Dropdown::widget([
                    //                        'items' => [
                    //                            ['label' => 'Action', 'url' => '#'],
                    //                            ['label' => 'Submenu', 'items' => [
                    //                                ['label' => 'Action', 'url' => '#'],
                    //                                ['label' => 'Another action', 'url' => '#'],
                    //                                ['label' => 'Something else here', 'url' => '#'],
                    //                            ]],
                    //                            ['label' => 'Something else here', 'url' => '#'],
                    //                            '<li class="divider"></li>',
                    //                            ['label' => 'Separated link', 'url' => '#'],
                    //                        ],
                    //                    ]);
                    ?>
                    <!--                    --><?php //Dropdown::end(); ?>
                </div>
            </div>

            <div class="panel panel-primary">
                <div class="panel-heading"><?php echo Yii::t('app', 'The cost and time'); ?></div>
                <div class="panel-body">

                    <div class="form-horizontal">
                        <?php echo $form->field($taskForm, 'price', [
                            'labelOptions' => ['class' => 'col-sm-3 control-label'],
                            'inputOptions' => ['class' => 'form-control text-right input-money'],
                            'errorOptions' => ['tag' => 'span'],
                            'template' => '{label}<div class="col-sm-9"><div class="input-group">{input}<span class="input-group-addon">руб.</span></div></div><div class="col-sm-12">{error}</div>',
                        ])->textInput(); ?>

                        <div class="form-group field-taskpriceform-deadlineNumPart required <?php echo $taskForm->hasErrors('deadlineNumPart') ? 'has-error' : ''; ?>">
                            <label for="taskpriceform-deadlineNumPart" class="col-sm-3 control-label"><?php echo Yii::t('app','Time'); ?></label>

                            <div class="col-sm-9">
                                <div class="input-group">
                                    <?php echo Html::activeTextInput($taskForm, 'deadlineNumPart', ['class' => 'form-control input-time-period text-right']); ?>

                                    <!--                                --><?php //echo $form->field($taskForm, 'deadlineNumPart', [
                                    //                                    'enableLabel' => false,
                                    //                                    'inputOptions' => ['class' => 'form-control input-time-period text-right'],
                                    //                                    'template' => '{input}',
                                    //                                ])->textInput();
                                    ?>

                                    <span class="input-group-addon">
                                        <?php echo Html::activeDropDownList($taskForm, 'deadlineTextPart', Task::$timeIntervalAliases, ['class' => 'form-control ']); ?>
                                    </span>
                                </div>
                                <?php echo Html::error($taskForm, 'deadlineNumPart', ['tag' => 'span', 'class' => 'help-block']); ?>
                            </div>
                        </div>
                        <?php if (!$model->isNewRecord): ?>
                            <?php echo Html::submitButton(Yii::t('app', 'Save'), [
                                'data-loading-text' => 'подождите...',
                                'class' => 'btn btn-primary btn-xs',
                                'name' => 'task_price_form',
                                'value' => 1
                            ]); ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>

        </div>
    </div>

<?php ActiveForm::end(); ?>