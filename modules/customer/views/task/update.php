<?php

use app\models\forms\TaskForm;
use app\models\TaskCategory;
use app\models\User;
use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Task $model
 * @var TaskForm $taskForm
 * @var TaskCategory[] $taskCategories
 * @var User $user
 */

$this->title = mb_strlen($model->title) > 0 ? $model->title : Yii::t('app','New task');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app','My tasks'), 'url' => ['task/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="task-update">

    <h1>
        <?= Html::encode($this->title) ?>
        <?php if (!$model->isNewRecord): ?>
            <?php echo Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger  pull-right',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure?'),
                    'method' => 'post',
                ],
            ]) ?>
        <?php endif; ?>
    </h1>

    <?= $this->render('_form', [
        'model' => $model,
        'taskForm' => $taskForm,
        'user' => $user,
    ]) ?>

</div>
