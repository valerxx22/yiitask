<?php

use app\models\Task;
use app\models\User;
use yii\helpers\Html;
use yii\grid\GridView;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\search\TaskSearch $searchModel
 * @var Task $model
 */

$this->title = Yii::t('app', 'My tasks');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="task-index">

    <h1>
        <?= Html::encode($this->title) ?>
    </h1>

    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'layout' => "{items}\n{pager}",
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            /*'id',*/
            [
                'attribute' => 'title',
                'filter' => false,
            ],
            /*[
                'attribute' => 'description',
                'format' => 'html',
                'filter' => false,
            ],*/
            [
                'attribute' => 'price',
//                'format' => 'number',
                'filter' => false,
            ],
            /*[
                'attribute' => 'negotiatedPrice',
                'value' => function ($model, $index) {
                        return $model->getNegotiatedPriceValueAlias();
                    },
                'filter' => Task::getBoolAliases(),
            ],*/
            [
                'attribute' => 'deadlineNumPart',
                'filter' => false,
            ],
            [
                'attribute' => 'deadlineTextPart',
                'value' => function ($model, $index) {
                        return $model->getDeadlineTextPartAlias();
                    },
                'filter' => false,
            ],
            [
                'attribute' => 'authorId',
                'filter' => false,
            ],
//            [
//                'attribute' => 'performerId',
//                'value' => function($model, $index) {
//                        /** @var Task $model */
//                        /** @var User|null $performer */
//                        $performer = $model->getPerformer()->one();
//                        return $performer ? $performer->getFullName() . ' [' . $performer->username . ']' : '-';
//                    },
//                'filter' => false,
//            ],
            [
                'attribute' => 'status',
                'value' => function($model, $index) {
                        return $model->getStatusAlias();
                    },
                'filter' => $model->getStatusAliases()
            ],
            /*[
                'attribute' => 'createdDate',
                'filter' => false,
            ],*/
            [
                'attribute' => 'lastUpdatedDate',
                'format' => 'datetime',
                'filter' => false,
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update}',
                'buttons' => [
                    'update' => function($url, $model) {
                            return '<a data-pjax="0" title="Просмотр" href="' . $url . '"><span class="glyphicon glyphicon-eye-open"></span></a>';
                        }
                ]
            ],
        ],
        'rowOptions' => function($model, $key, $index, $grid) {
                /** @var Task $model */
                switch($model->status) {
                    case Task::STATUS_NEW:
                        $cssClass = '';
                        break;
                    case Task::STATUS_REOPENED:
                        $cssClass = 'warning';
                        break;
                    case Task::STATUS_CLOSED:
                        $cssClass = 'active';
                        break;
                    default:
                        $cssClass = 'success';
                        break;
                }
                return ['class' => $cssClass];
            },
        'tableOptions' => [
            'class' => 'table table-bordered'
        ],
    ]); ?>

</div>
