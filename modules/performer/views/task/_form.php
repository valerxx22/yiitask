<?php

use app\models\forms\TaskForm;
use app\models\Task;
use app\models\User;
use itsurka\simpleComment\SimpleComment;
use mihaildev\ckeditor\CKEditor;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Task $model
 * @var TaskForm $taskForm
 * @var User $user
 */

// todo удалить
require_once (Yii::getAlias('@runtime') . "/tmp-extensions/yii2-simple-comment/SimpleComment.php");
?>

<!--    --><?php //echo $form->errorSummary([$taskForm]); ?>

    <div class="row show-grid">
        <div class="col-md-9">
            <div class="panel panel-primary">
                <div class="panel-heading"><?php echo Yii::t('app', 'Task'); ?></div>
                <div class="panel-body">
                    <table class="table">
                        <tbody>
                        <tr>
                            <th style="width: 200px;"><?php echo $taskForm->getAttributeLabel('title'); ?></th>
                            <td>
                                <div class="form-group"><?php echo Html::encode($taskForm->title); ?></div>
                            </td>
                        </tr>
                        <tr>
                            <th><?php echo $taskForm->getAttributeLabel('description'); ?></th>
                            <td>
                                <div class="form-group"><?php echo $taskForm->description; ?></div>
                            </td>
                        </tr>
                        <tr>
                            <th><?php echo $model->getAttributeLabel('createdDate'); ?></th>
                            <td><?php echo $model->createdDate; ?></td>
                        </tr>
                        <tr>
                            <th><?php echo $model->getAttributeLabel('lastUpdatedDate'); ?></th>
                            <td><?php echo $model->lastUpdatedDate; ?></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="panel panel-primary">
                <div class="panel-heading"><?php echo Yii::t('app', 'Comments'); ?></div>
                <div class="panel-body">

                    <?php echo SimpleComment::widget([
                        'model' => $model,
                        'author' => $user,
                        'authorNameAttribute' => 'username',
                    ]); ?>

                    <?php
                    /*use miloschuman\highcharts\Highcharts;

                    echo Highcharts::widget([
                        'options' => [
                            'title' => ['text' => 'Fruit Consumption'],
                            'xAxis' => [
                                'categories' => ['Apples', 'Bananas', 'Oranges']
                            ],
                            'yAxis' => [
                                'title' => ['text' => 'Fruit eaten']
                            ],
                            'series' => [
                                ['name' => 'Jane', 'data' => [1, 0, 4]],
                                ['name' => 'John', 'data' => [5, 7, 3]]
                            ]
                        ]
                    ]);*/
                    ?>
                </div>
            </div>
        </div>
        <div class="col-md-3">

            <?php $form = ActiveForm::begin([
                'enableClientValidation' => false
            ]); ?>

                <div class="panel panel-primary">
                    <div class="panel-heading"><?php echo Yii::t('app', 'Performer'); ?></div>
                    <div class="panel-body">
                        <i><?php echo Html::encode($model->performer->getFullName()); ?></i>
                    </div>
                </div>
                <div class="panel panel-primary">
                    <div class="panel-heading"><?php echo Yii::t('app', 'Status'); ?></div>
                    <div class="panel-body">
                        <?php echo $form->field($taskForm, 'status', ['enableLabel' => false])->dropDownList($model->getStatusAliases(User::TYPE_PERFORMER)); ?>

                        <?php echo Html::submitButton(Yii::t('app', 'Save'), [
                            'data-loading-text' => 'подождите...',
                            'class' => 'btn btn-primary btn-xs',
                            'name' => 'status_form',
                            'value' => 1
                        ]); ?>
                    </div>
                </div>

            <?php ActiveForm::end(); ?>

            <div class="panel panel-primary">
                <div class="panel-heading"><?php echo Yii::t('app', 'The cost and time'); ?></div>
                <div class="panel-body">

                    <table class="table">
                        <tr>
                            <td><b><?php echo $model->getAttributeLabel('price'); ?></b></td>
                            <td><?php echo Html::encode($model->getFancy('price')); ?></td>
                        </tr>
                        <tr>
                            <td><b><?php echo Yii::t('app','Time'); ?></b></td>
                            <td><?php echo Html::encode($model->getFancy('deadline')); ?></td>
                        </tr>
                    </table>

<!--                    <div class="form-horizontal">-->
<!--                        <div class="form-group">-->
<!--                            --><?php //echo Html::activeLabel($model, 'price', ['class' => 'col-sm-3 control-label']); ?>
<!--                            <div class="col-sm-9">-->
<!--                                --><?php //echo Html::encode($model->price); ?><!-- руб.-->
<!--                            </div>-->
<!--                        </div>-->
<!---->
<!--                        <div class="form-group">-->
<!--                            --><?php //echo Html::activeLabel($model, 'deadlineNumPart', ['class' => 'col-sm-3 control-label', 'label' => 'Срок']); ?>
<!---->
<!--                            <div class="col-sm-9">-->
<!--                                --><?php //echo $model->deadlineNumPart; ?><!-- --><?php //echo $model->getDeadlineTextPartAlias(); ?>
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
                </div>
            </div>
        </div>
    </div>
