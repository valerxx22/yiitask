<?php

use app\models\forms\TaskForm;
use app\models\User;
use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Task $model
 * @var TaskForm $taskForm
 * @var User $user
 */

$this->title = mb_strlen($model->title) > 0 ? $model->title : 'Новая задача';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app','My tasks'), 'url' => ['task/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="task-update">

    <?= $this->render('_form', [
        'model' => $model,
        'taskForm' => $taskForm,
        'user' => $user,
    ]) ?>

</div>
