<?php

namespace app\modules\performer;

class Performer extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\performer\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
