<?php

namespace app\modules\performer\controllers;

use app\controllers\FrontController;

class DefaultController extends FrontController
{
    public function actionIndex()
    {
        return $this->render('index');
    }
}
