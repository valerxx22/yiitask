<?php

namespace app\modules\performer\controllers;

use app\controllers\FrontController;
use app\models\forms\TaskForm;
use Yii;
use app\models\Task;
use app\models\search\TaskSearch;
use yii\db\Expression;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

class TaskController extends FrontController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new TaskSearch;
        Yii::$app->request->setQueryParams(['TaskSearch' => ['performerId' => $this->user->id]]);
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'model' => new Task(),
        ]);
    }

    public function actionUpdate($id = null)
    {
        // todo RBAC - can update this task!
        $model = $this->findModel($id);

        $request = Yii::$app->request;

        $taskForm = new TaskForm();
        $taskForm->title = $model->title;
        $taskForm->description = $model->description;
        $taskForm->status = $model->status;

        if ($request->isPost) {

            // обработка статуса
            if ($request->post('status_form')) {
                $taskForm->status = $request->post('TaskForm')['status'];
                if ($taskForm->validate(['status'])) {
                    $model->status = $taskForm->status;
                }
            }

            if (!$taskForm->hasErrors()) {
                $model->lastUpdatedDate = new Expression('NOW()');
                $model->save();
                $this->refresh();
            }
        }

        return $this->render('update', [
            'model' => $model,
            'taskForm' => $taskForm,
            'user' => $this->user,
        ]);
    }

    /**
     * Finds the Task model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Task the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Task::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
