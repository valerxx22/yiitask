<?php

namespace app\commands;

use app\models\User;
use yii\console\Controller;

class RbacController extends Controller
{
    public function actionInit()
    {
        $auth = \Yii::$app->authManager;

        // добавляем "createTask" разрешение
        $createTask = $auth->createPermission('createTask');
        $createTask->description = 'создать задачу';
        $auth->add($createTask);

        // добавляем "updateTask" разрешение
        $updateTask = $auth->createPermission('updateTask');
        $updateTask->description = 'обновить задачу';
        $auth->add($updateTask);

        // добавляем "deleteTask" разрешение
        $deleteTask = $auth->createPermission('deleteTask');
        $deleteTask->description = 'удалить задачу';
        $auth->add($deleteTask);

        // добавляем "setOpenedStatusTask" разрешение
        $setOpenedStatusTask = $auth->createPermission('setOpenedStatusTask');
        $setOpenedStatusTask->description = 'поставить статус задачи "открытая"';
        $auth->add($setOpenedStatusTask);

        // добавляем "setReopenedStatusTask" разрешение
        $setReopenedStatusTask = $auth->createPermission('setReopenedStatusTask');
        $setReopenedStatusTask->description = 'поставить статус задачи "переоткрытая"';
        $auth->add($setReopenedStatusTask);

        // добавляем "setInProgressStatusTask" разрешение
        $setInProgressStatusTask = $auth->createPermission('setInProgressStatusTask');
        $setInProgressStatusTask->description = 'поставить статус задачи "в процессе"';
        $auth->add($setInProgressStatusTask);

        // добавляем "setResolvedStatusTask" разрешение
        $setResolvedStatusTask = $auth->createPermission('setResolvedStatusTask');
        $setResolvedStatusTask->description = 'поставить статус задачи "решено"';
        $auth->add($setResolvedStatusTask);

        // добавляем "setVerifiedStatusTask" разрешение
        $setVerifiedStatusTask = $auth->createPermission('setVerifiedStatusTask');
        $setVerifiedStatusTask->description = 'поставить статус задачи "проверено"';
        $auth->add($setVerifiedStatusTask);

        // добавляем "setClosedStatusTask" разрешение
        $setClosedStatusTask = $auth->createPermission('setClosedStatusTask');
        $setClosedStatusTask->description = 'поставить статус задачи "закрыта"';
        $auth->add($setClosedStatusTask);

        // добавляем "customer" (заказчик) роль
        $customer = $auth->createRole('customer');
        $customer->description = 'заказчик';
        $auth->add($customer);
        $auth->addChild($customer, $createTask);
        $auth->addChild($customer, $updateTask);
        $auth->addChild($customer, $deleteTask);
        $auth->addChild($customer, $setReopenedStatusTask);
        $auth->addChild($customer, $setVerifiedStatusTask);
        $auth->addChild($customer, $setClosedStatusTask);

        // добавляем "performer" (исполнитель) роль
        $performer = $auth->createRole('performer');
        $performer->description = 'исполнитель';
        $auth->add($performer);
        $auth->addChild($performer, $setOpenedStatusTask);
        $auth->addChild($performer, $setInProgressStatusTask);
        $auth->addChild($performer, $setResolvedStatusTask);

        // привязываем роли у пользователям
        $customers = User::getCustomers();
        foreach ($customers as $eachCustomer) {
            $auth->assign($customer, $eachCustomer->id);
        }
        $performers = User::getPerformers();
        foreach ($performers as $eachPerformer) {
            $auth->assign($performer, $eachPerformer->id);
        }
    }

    public function actionExample()
    {
        $auth = \Yii::$app->authManager;

        // add "createPost" permission
        $createPost = $auth->createPermission('createPost');
        $createPost->description = 'create a post';
        $auth->add($createPost);

        // add "readPost" permission
        $readPost = $auth->createPermission('readPost');
        $readPost->description = 'read a post';
        $auth->add($readPost);

        // add "updatePost" permission
        $updatePost = $auth->createPermission('updatePost');
        $updatePost->description = 'update post';
        $auth->add($updatePost);

        // add "reader" role and give this role the "readPost" permission
        $reader = $auth->createRole('reader');
        $auth->add($reader);
        $auth->addChild($reader, $readPost);

        // add "author" role and give this role the "createPost" permission
        // as well as the permissions of the "reader" role
        $author = $auth->createRole('author');
        $auth->add($author);
        $auth->addChild($author, $createPost);
        $auth->addChild($author, $reader);

        // add "admin" role and give this role the "updatePost" permission
        // as well as the permissions of the "author" role
        $admin = $auth->createRole('admin');
        $auth->add($admin);
        $auth->addChild($admin, $updatePost);
        $auth->addChild($admin, $author);

        // Assign roles to users. 10, 14 and 26 are IDs returned by IdentityInterface::getId()
        // usually implemented in your User model.
        $auth->assign($reader, 10);
        $auth->assign($author, 14);
        $auth->assign($admin, 26);
    }

    public function actionClearall()
    {
        $auth = \Yii::$app->authManager;
        $auth->clearAll();
    }
}
