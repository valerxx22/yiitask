<?php
/**
 * User.php
 *
 * @author Igor Tsurka <turcaigor@gmail.com>
 * @date 12.08.14
 */

namespace app\helpers;

use yii\base\ErrorException;
use yii\swiftmailer\Mailer;

class User extends Mailer
{
    /**
     * @param $subject
     * @param $toEmail
     * @param $view
     * @param array $viewParams
     * @param null $fromEmail
     * @return bool
     * @throws \yii\base\ErrorException
     */
    public static function sendEmail($subject, $toEmail, $view, $viewParams = [], $fromEmail = null)
    {
        $toEmail = !YII_DEBUG ? $toEmail : \Yii::$app->params['testerEmail'];

        /** @var Mailer $mailer */
        $mailer = \Yii::$app->mailer;
        $result = $mailer
            ->compose('@app/views/mail/' . $view, $viewParams)
            ->setFrom($fromEmail)
            ->setTo($toEmail)
            ->setSubject($subject)
            ->send();

        if (!$result) {
            throw new ErrorException('Не удалось отправить письмо на ящик ' . $toEmail);
        }

        return $result;
    }
}