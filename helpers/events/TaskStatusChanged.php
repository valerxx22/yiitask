<?php
/**
 * TaskStatusChanged.php
 *
 * @author Igor Tsurka <turcaigor@gmail.com>
 * @date 13.08.14
 */

namespace app\helpers\events;

use app\models\Task;
use app\models\User;
use yii\base\Event;

/**
 * Class TaskStatusChanged
 * @package app\helpers\events
 */
class TaskStatusChanged extends Event
{
    /** @var  User */
    private $user;

    /** @var  Task */
    private $task;

    /** @var  integer */
    private $oldStatus;

    /** @var  integer */
    private $newStatus;

    /**
     * @param \app\models\User $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return \app\models\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param \app\models\Task $task
     */
    public function setTask($task)
    {
        $this->task = $task;
    }

    /**
     * @return \app\models\Task
     */
    public function getTask()
    {
        return $this->task;
    }

    /**
     * @param int $oldStatus
     */
    public function setOldStatus($oldStatus)
    {
        $this->oldStatus = $oldStatus;
    }

    /**
     * @return int
     */
    public function getOldStatus()
    {
        return $this->oldStatus;
    }

    /**
     * @param int $newStatus
     */
    public function setNewStatus($newStatus)
    {
        $this->newStatus = $newStatus;
    }

    /**
     * @return int
     */
    public function getNewStatus()
    {
        return $this->newStatus;
    }
}