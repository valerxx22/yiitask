<?php
/**
 * TaskAssign.php
 *
 * @author Igor Tsurka <turcaigor@gmail.com>
 * @date 12.08.14
 */

namespace app\helpers\events;

use app\models\Task;
use app\models\User;
use yii\base\Event;

class TaskAssign extends Event
{
    /** @var  User */
    private $performer;

    /** @var  User */
    private $customer;

    /** @var  Task */
    private $task;

    /**
     * @param \app\models\User $performer
     */
    public function setPerformer($performer)
    {
        $this->performer = $performer;
        return $this;
    }

    /**
     * @return \app\models\User
     */
    public function getPerformer()
    {
        return $this->performer;
    }

    /**
     * @param \app\models\Task $task
     */
    public function setTask($task)
    {
        $this->task = $task;
        return $this;
    }

    /**
     * @return \app\models\Task
     */
    public function getTask()
    {
        return $this->task;
    }

    /**
     * @param \app\models\User $customer
     */
    public function setCustomer($customer)
    {
        $this->customer = $customer;
        return $this;
    }

    /**
     * @return \app\models\User
     */
    public function getCustomer()
    {
        return $this->customer;
    }
} 