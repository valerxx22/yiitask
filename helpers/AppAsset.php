<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\helpers;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        'css/task.css',
        'js/jquery/jquery-ui-1.11.0.custom/jquery-ui.min.css',
    ];
    public $js = [
        'js/main.js',
        'js/task.js',
        'js/jquery/plentz-jquery-maskmoney-89c45c8/dist/jquery.maskMoney.min.js',
        'js/jquery/jquery.maskedinput.min.js',
        'js/jquery/jquery-ui-1.11.0.custom/jquery-ui.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset'
    ];
}
