<?php
/**
 * taskCategoriesTree.php
 *
 * @var yii\web\View $this
 * @var TaskCategory[] $taskCategories
 * @var Model $model
 * @var string $attributeName
 * @var string $menuCssClass
 *
 * @author Igor Tsurka <turcaigor@gmail.com>
 * @date 28.07.14
 */

use app\models\TaskCategory;
use yii\base\Model;
use yii\helpers\Html;

if (Yii::$app->request->isAjax) {
    $registerTaskJs = \Yii::$app->assetManager->publish(\Yii::getAlias('@webroot') . '/js/task.js', ['forceCopy' => YII_DEBUG])[1];
    $this->registerJsFile($registerTaskJs . '?random=' . uniqid());
}
?>

<ul class="task-categories-tree <?php echo $menuCssClass; ?>">
    <?php foreach ($taskCategories as $eachTaskCategory): ?>
        <li class="tree-item">
            <?php $_fieldName = $attributeName.'[' . $eachTaskCategory->id . ']'; ?>
            <?php echo Html::activeCheckbox($model, $_fieldName); ?>
            <?php echo Html::activeLabel($model, $_fieldName, ['label' => $eachTaskCategory->nameRu]); ?>
            <ul class="tree-child <?php echo !empty($model->{$attributeName}[$eachTaskCategory->id]) ? 'active' : ''; ?>">
                <?php foreach ($eachTaskCategory->children as $eachChildTaskCategory): ?>
                    <?php $_fieldName = $attributeName . '[' . $eachChildTaskCategory->id . ']'; ?>
                    <li class="tree-item">
                        <?php echo Html::activeCheckbox($model, $_fieldName); ?>
                        <?php echo Html::activeLabel($model, $_fieldName, ['label' => $eachChildTaskCategory->nameRu]); ?>
                    </li>
                <?php endforeach; ?>
            </ul>
        </li>
    <?php endforeach; ?>
</ul>