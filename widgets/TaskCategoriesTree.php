<?php
/**
 * TaskCategoriesTree.php
 *
 * @author Igor Tsurka <turcaigor@gmail.com>
 * @date 28.07.14
 */

namespace app\widgets;


use app\models\TaskCategory;
use yii\base\Model;
use yii\base\Widget;
use yii\web\AssetBundle;

class TaskCategoriesTree extends Widget
{
    /**
     * @var Model
     */
    public $model;

    /**
     * @var string
     */
    public $attributeName;

    /**
     * @var string
     */
    public $menuCssClass;

    public function run()
    {
        // select only active and first-level categories
        /** @var TaskCategory $taskCategories */
        $taskCategories = TaskCategory::find()->where([
            'active' => 1,
            'parentId' => 0,
        ])->all();

        $function = new \ReflectionClass(get_class($this->model));

        if (isset($_REQUEST[$function->getShortName()])) {
            foreach ($taskCategories as $eachCategory) {
                $this->model->{$this->attributeName}[$eachCategory->id] = !empty($_REQUEST[$function->getShortName()][$this->attributeName][$eachCategory->id]) ? 1 : 0;
            }
        }

        return $this->render('taskCategoriesTree', [
            'taskCategories' => $taskCategories,
            'model' => $this->model,
            'attributeName' => $this->attributeName,
            'menuCssClass' => $this->menuCssClass,
        ]);
    }
}