<?php
/** @var array $local_config */

//var_dump('z', $local_config);
//exit;

return [
    'class' => 'yii\db\Connection',
    'dsn' => $local_config['db']['dsn'],
    'username' => $local_config['db']['username'],
    'password' => $local_config['db']['password'],
    'charset' => 'utf8',
];