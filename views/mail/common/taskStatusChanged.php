<?php
/**
 * @var yii\web\View $this
 * @var User $user
 * @var Task $task
 * @var integer $oldStatus
 * @var integer $newStatus
 */
use app\models\Task;
use app\models\User;
?>

<h4>Привет, <?php echo $user->getReallyFullName(); ?>!</h4>

<p>
    У задачи <?php echo \yii\helpers\Html::a($task->title, $task->getViewUrl()); ?> сменился статус с "<?php echo Task::getStatuses($oldStatus); ?>" на "<?php echo Task::getStatuses($newStatus); ?>".
</p>