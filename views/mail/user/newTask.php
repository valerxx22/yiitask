<?php
/**
 * @var yii\web\View $this
 * @var User $performer
 * @var User $customer
 * @var Task $task
 */
use app\models\Task;
use app\models\User;
?>

<h4>Привет, <?php echo $performer->getReallyFullName(); ?>!</h4>

<p>
    Заказчик <?php echo \yii\helpers\Html::a($customer->getReallyFullName(), $customer->getProfileUrl()); ?> добавил тебе новую задачу - <?php echo \yii\helpers\Html::a($task->title, $task->getViewUrl()); ?>.
</p>