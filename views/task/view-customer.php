<?php

use app\models\forms\PerformerForm;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\widgets\DetailView;

/**
 * @var yii\web\View $this
 * @var app\models\Task $model
 * @var PerformerForm $performerForm
 */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Задачи', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="task-view">

    <h1><?= Html::encode($this->title) ?></h1>

<!--    --><?php //$form = ActiveForm::begin(['layout' => 'horizontal']); ?>
<!---->
<!--    --><?php //echo $form->field($model, 'title')->hint('sd'); ?>
<!---->
<!--    --><?php //ActiveForm::end(); ?>

    <div class="row show-grid">
        <div class="col-md-12">
            <?= Html::a(Yii::t('app', 'Редактировать'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('app', 'Удалить'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Вы уверены?'),
                    'method' => 'post',
                ],
            ]) ?>
        </div>
    </div>
    <div class="row show-grid">
        <div class="col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading">Задача</div>
                <div class="panel-body">
                    <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            'title',
                            'description:ntext',
                            'price',
                            'negotiatedPrice',
                            'deadlineNumPart',
                            'deadlineTextPart',
//                            'authorId',
//                            'performerId',
//                            'status',
                            'createdDate',
                            'lastUpdatedDate',
                        ],
                    ]) ?>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="panel panel-primary">
                <div class="panel-heading">Исполнитель</div>
                <div class="panel-body">

                    <?php $form = ActiveForm::begin([]); ?>

                    <span class="hidden">
                        <?php echo $form->field($performerForm, 'performerId', ['enableLabel' => false])->hiddenInput(); ?>
                    </span>

                    <?php echo $form->field($performerForm, 'performerName', [
                        'class' => 'yii\autocomplete\ActiveField',
                        'enableLabel' => false,
                        'options' => ['class' => ''],
                        'inputOptions' => [
                            'placeholder' => '',
                        ]])->autocompleteInput([
                            'clientOptions' => [
                                'serviceUrl' => 'performersSearch',
                                'minChars' => 3,
//                                'lookup' => ['January', 'February', 'March', 'April', 'May']
                            ]
                        ]); ?>

                    <?php echo Html::submitButton('Сохранить', ['class' => 'btn btn-primary']); ?>

                    <?php ActiveForm::end(); ?>

                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="panel panel-primary">
                <div class="panel-heading">Статус</div>
                <div class="panel-body">
                    <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            'status',
                        ],
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>
