<?php

use app\models\Task;
use yii\helpers\Html;
use yii\grid\GridView;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\search\TaskSearch $searchModel
 * @var Task $model
 */

$this->title = Yii::t('app', 'Tasks');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="task-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?=
        Html::a(Yii::t('app', 'Create {modelClass}', [
            'modelClass' => 'Task',
        ]), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            /*'id',*/
            [
                'attribute' => 'title',
                'filter' => false,
            ],
            /*[
                'attribute' => 'description',
                'format' => 'html',
                'filter' => false,
            ],*/
            [
                'attribute' => 'price',
//                'format' => 'number',
                'filter' => false,
            ],
            [
                'attribute' => 'negotiatedPrice',
                'value' => function ($model, $index) {
                        return $model->getNegotiatedPriceValueAlias();
                    },
                'filter' => Task::getBoolAliases(),
            ],
            [
                'attribute' => 'deadlineNumPart',
                'filter' => false,
            ],
            [
                'attribute' => 'deadlineTextPart',
                'value' => function ($model, $index) {
                        return $model->getDeadlineTextPartAlias();
                    },
                'filter' => false,
            ],
            /*[
                'attribute' => 'authorId',
                'filter' => false,
            ],*/
            [
                'attribute' => 'performerId',
                'filter' => false,
            ],
            [
                'attribute' => 'status',
                'value' => function($model, $index) {
                        return $model->getStatusAlias();
                    },
                'filter' => Task::getStatusAliases()
            ],
            /*[
                'attribute' => 'createdDate',
                'filter' => false,
            ],*/
            [
                'attribute' => 'lastUpdatedDate',
                'format' => 'datetime',
                'filter' => false,
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
        'rowOptions' => function($model, $key, $index, $grid) {
                /** @var Task $model */
                switch($model->status) {
                    case Task::STATUS_NEW:
                        $cssClass = '';
                        break;
                    case Task::STATUS_REOPENED:
                        $cssClass = 'warning';
                        break;
                    case Task::STATUS_CLOSED:
                        $cssClass = 'active';
                        break;
                    default:
                        $cssClass = 'success';
                        break;
                }
                return ['class' => $cssClass];
            },
        'tableOptions' => [
            'class' => 'table table-bordered'
        ],
    ]); ?>

</div>
