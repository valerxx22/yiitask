<?php

use app\models\forms\TaskRespondForm;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\widgets\DetailView;

/**
 * @var yii\web\View $this
 * @var app\models\Task $task
 * @var TaskRespondForm $taskRespondForm
 */

$this->title = $task->title;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="task-view">

    <div class="row">
        <div class="col-lg-9">
            <h1><?= Html::encode($this->title) ?></h1>
        </div>

        <div class="col-lg-9">

            <div class="panel panel-default">
                <div class="panel-body">
                    <?php echo $task->description; ?>
                </div>
            </div>

<!--            --><?//= DetailView::widget([
//                'model' => $task,
//                'attributes' => [
//                    'id',
//                    'title',
//                    'description:ntext',
//                    'price',
//                    'negotiatedPrice',
//                    'deadlineNumPart',
//                    'deadlineTextPart',
//                    'authorId',
//                    'performerId',
//                    'status',
//                    'createdDate',
//                    'lastUpdatedDate',
//                ],
//            ]) ?>
        </div>
        <div class="col-lg-3">
            <div class="panel panel-default">
                <div class="panel-heading">Информация</div>
                <div class="panel-body">
                    <table class="table">
                        <tr>
                            <td><b><?php echo $task->getAttributeLabel('authorId'); ?></b></td>
                            <td><?php echo Html::a($task->author->username, $task->author->getProfileUrl()); ?></td>
                        </tr>
                        <tr>
                            <td><b><?php echo $task->getAttributeLabel('price'); ?></b></td>
                            <td><?php echo Html::encode($task->getFancy('price')); ?></td>
                        </tr>
                        <tr>
                            <td><b>Срок</b></td>
                            <td><?php echo Html::encode($task->getFancy('deadline')); ?></td>
                        </tr>
                        <tr>
                            <td><b>Дата</b></td>
                            <td><?php echo Html::encode($task->getFancy('createdDate')); ?></td>
                        </tr>
                    </table>
                </div>
            </div>

            <?php if (Yii::$app->controller->user): ?>
                <?php $canRespondTaskObj = Yii::$app->controller->user->getCanRespondTask($task->id); ?>
                <?php if ($canRespondTaskObj->canRespond): ?>
                    <div class="text-center">
                        <?php echo $this->renderAjax('/blocks/taskRespondForm', ['taskRespondForm' => $taskRespondForm]); ?>
                    </div>
                <?php else: ?>
                    <?php if ($canRespondTaskObj->errorCode > 0): ?>
                        <p><?php echo $canRespondTaskObj->errorMessage; ?></p>
                    <?php endif; ?>
                <?php endif; ?>
            <?php else: ?>
                <div class="text-center">
                    <?php echo $this->renderAjax('/blocks/taskRespondForm', ['taskRespondForm' => $taskRespondForm]); ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>
