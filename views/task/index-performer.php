<?php

use app\models\Task;
use yii\grid\ActionColumn;
use yii\helpers\Html;
use yii\grid\GridView;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\search\TaskSearch $searchModel
 */

$this->title = Yii::t('app', 'Tasks');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="task-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?=
        Html::a(Yii::t('app', 'Create {modelClass}', [
            'modelClass' => 'Task',
        ]), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            'description:ntext',
            'price',
            'negotiatedPrice',
            'author.firstName',
             'deadlineNumPart',
             'deadlineTextPart',
            // 'performerId',
             [
                 'attribute' => 'status',
                 'filter' => Task::getStatusAliases()
             ],
            // 'createdDate',
            // 'lastUpdatedDate',

            [
                'class' => 'yii\grid\ActionColumn',
                'buttons' => [
                    'update' => function ($url, $model) {
                            /** @var Task $model */
                            if (\Yii::$app->user->can('updateTask')) {
                                return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                    'title' => Yii::t('yii', 'View'),
                                    'data-pjax' => '0',
                                ]);
                            }
                            return false;
                        },
                    'delete' => function ($url, $model) {
                            /** @var Task $model */
                            if (\Yii::$app->user->can('deleteTask')) {
                                return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                    'title' => Yii::t('yii', 'Delete'),
                                    'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                                    'data-method' => 'post',
                                    'data-pjax' => '0',
                                ]);
                            }
                            return false;
                        },
                ]
            ],
        ],
    ]); ?>

</div>
