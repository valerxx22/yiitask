<?php

use app\models\search\TaskSearch;
use app\models\Task;
use app\models\User;
use app\widgets\TaskCategoriesTree;
use yii\bootstrap\ActiveForm;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ListView;

/**
 * @var yii\web\View $this
 * @var Task $model
 * @var ActiveQuery $activeQuery
 * @var ActiveForm $form
 * @var TaskSearch $searchModel
 * @var ActiveDataProvider $dataProvider
 */

$this->title = 'Задачи';
?>
<div class="task-search">

    <div class="row">
        <div class="col-md-12">
            <h1>
                <?= Html::encode($this->title) ?>
            </h1>
        </div>
    </div>

    <div class="row">

        <?php \yii\widgets\Pjax::begin(); ?>

            <?php
            if (Yii::$app->request->isAjax) {
                $jsFilePath = \Yii::$app->assetManager->publish(\Yii::getAlias('@webroot') . '/js/main.js', ['forceCopy' => YII_DEBUG])[1];
                $this->registerJsFile($jsFilePath . '?random=' . uniqid());
            }
            ?>

            <?php $form = ActiveForm::begin(['options' => ['data-pjax' => ''], 'id' => 'search-task-form']); ?>

                <div class="col-md-9">

                    <div class="panel">
                        <div class="input-group">
                            <?php echo Html::activeTextInput($searchModel, 'title', ['class' => 'form-control']); ?>
                            <div class="input-group-btn">
                                <?php echo Html::button(Yii::t('app', 'Search'), ['class' => 'btn btn-default']); ?>
                            </div>
                        </div>
                    </div>


                    <div class="panel panel-default">
                        <div class="panel-heading"><?php echo \Yii::t('app', 'Found {n} tasks', ['n' => $dataProvider->totalCount]); ?></div>
                        <div class="panel-body">

                            <?php echo ListView::widget([
                                'layout' => "{items}",
                                'itemView' => '_task',
                                'options' => ['tag' => 'ul', 'class' => 'media-list task-list'],
                                'itemOptions' => ['tag' => 'li', 'class' => 'media task-list-item'],
                                'dataProvider' => $dataProvider
                            ]); ?>

                        </div>
                    </div>

                </div>

                <div class="col-md-3">

                    <div class="panel panel-default">
                        <div class="panel-heading">Категории</div>
                        <div class="panel-body">
                            <?php echo TaskCategoriesTree::widget([
                                'model' => $searchModel,
                                'attributeName' => 'categoryIDs',
                                'menuCssClass' => 'js-search-form',
                            ]); ?>
                        </div>
                    </div>

                </div>

            <?php ActiveForm::end(); ?>

        <?php \yii\widgets\Pjax::end(); ?>
    </div>


</div>
