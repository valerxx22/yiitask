<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/**
 * @var yii\web\View $this
 * @var app\models\Task $model
 */
?>

<h4 class="task-title">
    <a href="<?php echo $model->getViewUrl(); ?>" title="" data-pjax="0">
        <?php echo Html::encode($model->title); ?>
    </a>
</h4>
<div class="task-info">
    <div class="task-info-item">
        <span class="glyphicon glyphicon-eye-open"></span> <?php echo $model->getFancy('viewCount'); ?>
    </div>

    <div class="task-info-item">
        <span class="glyphicon glyphicon-check"></span> <?php echo $model->getFancy('responseCount'); ?>
    </div>
    <div class="task-info-item">
        <span class="glyphicon glyphicon-calendar"></span> <?php echo $model->getFancy('createdDate'); ?>
    </div>
</div>
<?php if ($model->hasTags()): ?>
    <div>
        <?php echo $model->getFancy('tagNames'); ?>
    </div>
<?php endif; ?>