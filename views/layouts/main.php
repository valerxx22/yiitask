<?php
use app\models\User;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\helpers\AppAsset;

/**
 * @var \yii\web\View $this
 * @var string $content
 */
AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>

<?php $this->beginBody() ?>
    <div class="wrap">
        <?php
        NavBar::begin([
            'brandLabel' => Yii::$app->params['projectName'],
            'brandUrl' => Yii::$app->homeUrl,
            'options' => [
                'class' => 'navbar-inverse navbar-fixed-top',
            ],
        ]);

        $menuItems = [
        ];

        if (Yii::$app->user->isGuest) {
            $menuItems[] = ['label' => \Yii::t('app', 'Sign In'), 'url' => ['/site/login']];
            $menuItems[] = ['label' => \Yii::t('app', 'Sign Up'), 'url' => ['/site/registration']];
        } else {
            /** @var User $userIdentity */
            $userIdentity = \Yii::$app->user->identity;

            if ($userIdentity->getIsCustomer()) {

                $newResponsesInfo = $userIdentity->getResponsesInfo();
                $responsesLabel = \Yii::t('app', 'Responses');
                if ($newResponsesInfo[\app\models\PerformerTaskRespond::STATUS_NEW]['count'] > 0) {
                    $responsesLabel .= ' <span class="badge">' . $newResponsesInfo[\app\models\PerformerTaskRespond::STATUS_NEW]['count'] . '</span>';
                }

                $menuItems[] = ['label' => $responsesLabel, 'url' => ['/customer/task/responses']];
                $menuItems[] = ['label' => \Yii::t('app', 'My tasks'), 'url' => ['/customer/task/index']];
            } elseif ($userIdentity->getIsPerformer()) {
                $menuItems[] = ['label' => \Yii::t('app', 'Search tasks'), 'url' => ['/task/search']];
                $menuItems[] = ['label' => \Yii::t('app', 'My tasks'), 'url' => ['/performer/task/index']];
            }
            $menuItems[] = ['label' => \Yii::t('app', 'Sign Out'), 'url' => ['/site/logout'], 'linkOptions' => ['data-method' => 'post']];
        }

        echo Nav::widget([
            'options' => ['class' => 'navbar-nav navbar-right'],
            'items' => $menuItems,
            'encodeLabels' => false,
        ]);
        NavBar::end();
        ?>

        <div class="container">
            <?= Breadcrumbs::widget([
                'homeLink' => [
                    'label' => 'Главная',
                    'url' => Yii::$app->homeUrl,
                ],
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
            <?= $content ?>
        </div>
    </div>

    <footer class="footer">
        <div class="container">
            <p class="pull-left">&copy; My Company <?= date('Y') ?></p>
            <p class="pull-right"><?= Yii::powered() ?></p>
        </div>
    </footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
