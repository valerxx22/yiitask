<?php

require_once (Yii::getAlias('@runtime') . "/tmp-extensions/yii2-simple-comment/SimpleComment.php");

use itsurka\simpleComment\SimpleComment;

/**
 * @var yii\web\View $this
 */

$task = \app\models\Task::find()->one();
?>

<?php echo SimpleComment::widget([
    'model' => $task,
]); ?>

<?php
use miloschuman\highcharts\Highcharts;

echo Highcharts::widget([
    'options' => [
        'title' => ['text' => 'Fruit Consumption'],
        'xAxis' => [
            'categories' => ['Apples', 'Bananas', 'Oranges']
        ],
        'yAxis' => [
            'title' => ['text' => 'Fruit eaten']
        ],
        'series' => [
            ['name' => 'Jane', 'data' => [1, 0, 4]],
            ['name' => 'John', 'data' => [5, 7, 3]]
        ]
    ]
]);
?>