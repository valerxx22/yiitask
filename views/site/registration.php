<?php
use app\models\User;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var yii\widgets\ActiveForm $form
 * @var app\models\LoginForm $model
 */
$this->title = Yii::t('app', 'Registration');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>Please fill out the following fields to login:</p>

    <?php $form = ActiveForm::begin([
        'id' => 'registration-form',
        'options' => ['class' => 'form-horizontal'],
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
            'labelOptions' => ['class' => 'col-lg-1 control-label'],
        ],
        'enableClientValidation' => false
    ]); ?>

    <?php echo $form->field($model, 'email')->textInput(); ?>
    <?php echo $form->field($model, 'username')->textInput(); ?>
    <?php echo $form->field($model, 'passwordHash')->passwordInput(); ?>
    <?php echo $form->field($model, 'firstName')->textInput(); ?>
    <?php echo $form->field($model, 'lastName')->textInput(); ?>
    <?php echo $form->field($model, 'patronymic')->textInput(); ?>
    <?php echo $form->field($model, 'phone')->textInput(); ?>
    <?php echo $form->field($model, 'type')->radioList(User::getTypeAliases()); ?>

    <div class="form-group">
        <div class="col-lg-offset-1 col-lg-11">
            <?= Html::submitButton('Sign Up', ['class' => 'btn btn-primary', 'name' => 'registration-button']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>
