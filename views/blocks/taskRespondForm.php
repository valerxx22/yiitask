<?php
/**
 * @var TaskRespondForm $taskRespondForm
 */
use app\models\forms\TaskRespondForm;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

?>

<?php $form = ActiveForm::begin(['options' => ['data-pjax' => '']]); ?>
<?php echo Html::activeHiddenInput($taskRespondForm, 'userId'); ?>
<?php if ($taskRespondForm->hasErrors('userId')): ?>
    <p class="bg-danger"><?php echo $taskRespondForm->getFirstError('userId'); ?></p>
<?php endif; ?>
<?php echo Html::button(Yii::t('app', 'Respond'), ['class' => 'btn btn-success']); ?>
<?php ActiveForm::end(); ?>