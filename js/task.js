/**
 * Created by itsurka on 26.07.14.
 */

$(function() {

    // дерево категорий задач, обработка кликов на чекбоксы
    $('.task-categories-tree input:checkbox').click(function() {
        var $currentCheckbox = $(this);
        var $treeItem = $currentCheckbox.closest('.tree-item');

        var $treeChildExt = $treeItem.closest('.tree-child').eq(0);
        var $treeChildInt = $treeItem.find('.tree-child').eq(0);
        var $treeParent = $treeItem.parent().closest('.tree-item').eq(0);
        var $childrenCheckboxes = $treeChildInt.find('input:checkbox');
        var $currentCheckboxIsChild = $treeChildExt.length == 1;

        $childrenCheckboxes.prop('checked', !$(this).is(':checked'));

        // parent checkbox
        if ($treeChildInt.length) {
            if ($treeChildInt.hasClass('active') && $currentCheckbox.is(':checked')) {
                $currentCheckbox.prop('checked', false);
            }
            $treeChildInt.toggleClass('active');

            // check all children
            if ($treeChildInt.hasClass('active') && $treeChildInt.find('input:checkbox:checked').length == 0) {
                $treeChildInt.find('input:checkbox').prop('checked', true);
            } else if (!$treeChildInt.hasClass('active')) {
                $treeChildInt.find('input:checkbox').prop('checked', false);
            }
        }

        // child checkbox
        if ($currentCheckboxIsChild) {
            $treeParent.find('input:checkbox').eq(0).prop('checked', $treeChildExt.find('input:checkbox:checked').length > 0);
        }
    });

});