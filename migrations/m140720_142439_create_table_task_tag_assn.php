<?php

use yii\db\Schema;

class m140720_142439_create_table_task_tag_assn extends \yii\db\Migration
{
    public function up()
    {
        $this->createTable('tbl_task_tag_assn', [
            'task_id' => 'integer',
            'tag_id' => 'integer',
        ]);
    }

    public function down()
    {
        $this->dropTable('tbl_task_tag_assn');
    }
}
