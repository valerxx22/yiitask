<?php

use yii\db\Schema;

class m140725_162938_add_new_categories extends \yii\db\Migration
{
    public function up()
    {
        $this->insert('task_category', [
            'nameRu' => 'Версия фреймворка',
            'nameEn' => 'Framework version',
        ]);

        $this->insert('task_category', [
            'nameRu' => '1.1.x',
            'nameEn' => '1.1.x',
            'parentId' => $this->db->createCommand('select id from task_category where nameRu = "Версия фреймворка"')->queryScalar(),
        ]);

        $this->insert('task_category', [
            'nameRu' => '2.x',
            'nameEn' => '2.x',
            'parentId' => $this->db->createCommand('select id from task_category where nameRu = "Версия фреймворка"')->queryScalar(),
        ]);

        $this->insert('task_category', [
            'nameRu' => 'Модуль',
            'nameEn' => 'Module',
        ]);

        $this->insert('task_category', [
            'nameRu' => 'Расширение',
            'nameEn' => 'Extension',
        ]);

        $this->insert('task_category', [
            'nameRu' => 'Форма',
            'nameEn' => 'Form',
        ]);

        $this->insert('task_category', [
            'nameRu' => 'Виджет',
            'nameEn' => 'Widget',
        ]);
    }

    public function down()
    {
        $this->delete('task_category', "nameRu IN ('Версия фреймворка', '1.1.x', '2.x', 'Модуль', 'Расширение', 'Форма', 'Виджет')");
    }
}
