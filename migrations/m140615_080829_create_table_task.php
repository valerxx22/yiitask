<?php

use yii\db\Schema;

class m140615_080829_create_table_task extends \yii\db\Migration
{
    public function up()
    {
        $this->createTable('task', [
            'id' => 'pk',
            'title' => 'varchar(255)',
            'description' => 'text null',
            'price' => 'int unsigned',
            'negotiatedPrice' => "tinyint(1) default '0'",
            'deadlineNumPart' => "tinyint(2) unsigned",
            'deadlineTextPart' => "enum('hour', 'day', 'week', 'month')",
            'authorId' => 'int',
            'performerId' => 'int',
            'responseCount' => 'integer not null default 0',
            'viewCount' => 'integer not null default 0',
            'uniqueViewCount' => 'integer not null default 0',
            'status' => "enum('new', 'opened', 'inProgress', 'resolved', 'verified', 'closed', 'reopened') default 'new'",
            'createdDate' => 'timestamp not null default now()',
            'lastUpdatedDate' => 'timestamp null',
        ]);

        $this->addForeignKey(
            'FK_Task_authorId',
            'task', 'authorId',
            'user', 'id',
            'CASCADE', 'CASCADE'
        );

        $this->addForeignKey(
            'FK_Task_performerId',
            'task', 'performerId',
            'user', 'id',
            'CASCADE', 'CASCADE'
        );

    }

    public function down()
    {
        $this->dropTable('task');
    }
}
