<?php

use yii\db\Schema;

class m140725_155754_create_table_task_category extends \yii\db\Migration
{
    public function up()
    {
        $this->createTable('task_category', [
            'id' => 'pk',
            'nameRu' => 'string not null',
            'nameEn' => 'string not null',
            'parentId' => 'integer not null default 0',
            'active' => "tinyint(1) not null default '1'",
            'createdDate' => 'timestamp not null default now()',
        ]);
    }

    public function down()
    {
        $this->dropTable('task_category');
    }
}
