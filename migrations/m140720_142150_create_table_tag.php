<?php

use yii\db\Schema;

class m140720_142150_create_table_tag extends \yii\db\Migration
{
    public function up()
    {
        $this->createTable('tbl_tag', [
            'id' => 'pk',
            'frequency' => 'integer not null default \'0\'',
            'name' => 'varchar(25)',
            'createdDate' => 'timestamp not null default now()',
        ]);
    }

    public function down()
    {
        $this->dropTable('tbl_tag');
    }
}
