<?php

use app\models\User;
use yii\db\Schema;
use yii\helpers\Security;

class m140626_152316_add_test_users extends \yii\db\Migration
{
    public function up()
    {
        $this->insert(User::tableName(), [
            'email' => 'consumer1@test.te',
            'username' => 'customer1',
            'phone' => '0079031234567',
            'firstName' => 'Цурка',
            'lastName' => 'Игорь',
            'patronymic' => 'Васильевич',
            'type' => User::TYPE_CUSTOMER,
            'passwordHash' => Security::generatePasswordHash('123456'),
        ]);

        $this->insert(User::tableName(), [
            'email' => 'consumer2@test.te',
            'username' => 'customer2',
            'phone' => '0079031234555',
            'firstName' => 'Дмитрий',
            'lastName' => 'Славинский',
            'patronymic' => 'Батькевич',
            'type' => User::TYPE_CUSTOMER,
            'passwordHash' => Security::generatePasswordHash('123456'),
        ]);

        $this->insert(User::tableName(), [
            'email' => 'performer1@test.te',
            'username' => 'performer1',
            'phone' => '0079031234444',
            'firstName' => 'Андрей',
            'lastName' => 'Митин',
            'patronymic' => 'Алексеевич',
            'type' => User::TYPE_PERFORMER,
            'passwordHash' => Security::generatePasswordHash('123456'),
        ]);

        $this->insert(User::tableName(), [
            'email' => 'performer2@test.te',
            'username' => 'performer2',
            'phone' => '0079031234444',
            'firstName' => 'Александр',
            'lastName' => 'Самдарк',
            'patronymic' => 'Батькевич',
            'type' => User::TYPE_PERFORMER,
            'passwordHash' => Security::generatePasswordHash('123456'),
        ]);
    }

    public function down()
    {
        $this->delete(User::tableName(), "email IN ('customer1@test.te', 'customer2@test.te', 'performer1@test.te', 'performer2@test.te')");
    }
}
