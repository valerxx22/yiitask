<?php

use yii\db\Schema;

class m140615_075401_create_table_user extends \yii\db\Migration
{
    public function up()
    {
        $this->createTable('user', [
            'id' => 'pk',
            'email' => 'varchar(50)',
            'username' => 'varchar(25)',
            'phone' => 'varchar(50) null',
            'firstName' => 'varchar(25)',
            'lastName' => 'varchar(25)',
            'patronymic' => 'varchar(25)',
            'type' => "enum('customer', 'performer')",
            'passwordHash' => 'varchar(100)',
            'authKey' => 'varchar(100) null',
            'accessToken' => 'varchar(100) null',
            'createdDate' => 'timestamp not null default now()',
        ]);

        $this->createIndex('IDX_User_email_unique', 'user', 'email');
        $this->createIndex('IDX_User_username_unique', 'user', 'username');
    }

    public function down()
    {
        $this->dropTable('user');
    }
}
