<?php

use yii\db\Schema;

class m140724_171418_create_table_performer_task_respond extends \yii\db\Migration
{
    public function up()
    {
        $this->createTable('performer_task_respond', [
            'id' => 'pk',
            'taskId' => 'integer not null',
            'performerId' => 'integer not null',
            'status' => "tinyint(1) not null default '0'",
            'createdDate' => 'timestamp not null default now()',
        ]);

        $this->addForeignKey(
            'FK_PerformerTaskRespond_taskId',
            'performer_task_respond', 'taskId',
            'task', 'id',
            'CASCADE', 'CASCADE'
        );

        $this->addForeignKey(
            'FK_PerformerTaskRespond_performerId',
            'performer_task_respond', 'performerId',
            'user', 'id',
            'CASCADE', 'CASCADE'
        );
    }

    public function down()
    {
        $this->dropTable('performer_task_respond');
    }
}
