<?php

use yii\db\Schema;

class m140725_161105_create_table_task_category_assn extends \yii\db\Migration
{
    public function up()
    {
        $this->createTable('task_category_assn', [
            'taskId' => 'integer',
            'categoryId' => 'integer',
        ]);

        $this->addForeignKey(
            'FK_TaskCategoryAssn_taskId',
            'task_category_assn', 'taskId',
            'task', 'id',
            'CASCADE', 'CASCADE'
        );

        $this->addForeignKey(
            'FK_TaskCategoryAssn_categoryId',
            'task_category_assn', 'categoryId',
            'task_category', 'id',
            'CASCADE', 'CASCADE'
        );
    }

    public function down()
    {
        $this->dropTable('task_category_assn');
    }
}
